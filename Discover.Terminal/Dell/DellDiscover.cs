﻿using Discover.Solutions.Web.Spider;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellDiscover
    {
        private IDellDiscoverService _service;

        public IDellDiscoverService Service
        {
            get { return _service ?? (_service = IoC.GetInstance<IDellDiscoverService>()); }
        }
        
        public void Download(string url, string path)
        {
            var dellService = DellDiscoverService.Open(path, new WebSpiderProject { BaseUrl = url, StayOnSite = true, MaximumEntryLevel = 5, ProxyUsage = DownloadProxyUsage.NoProxy });

            dellService.ProcessingUrl += new ProcessingUrlEventHandler(downloader_ProcessingUrl);

            dellService.ProcessCompleted += new ProcessCompletedEventHandler(downloader_ProcessCompleted);

            dellService.DownloadAsync();

            while (true)
            {
                Thread.Sleep(1000);
                Console.WriteLine(@".");

                lock (typeof(Program))
                {
                    if (finished)
                    {
                        break;
                    }
                }
            }

            Console.WriteLine(@"finished.");
        }

        private static void downloader_ProcessingUrl(object sender, ProcessingUrlEventArgs e)
        {
            Console.WriteLine(@"Processing URL '{0}'.", e.Entry.RemoteUrl);
        }

        private static bool finished;

        private static void downloader_ProcessCompleted(
            object sender,
            RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Console.WriteLine(@"Error: " + e.Error.Message);
            }
            else if (e.Cancelled)
            {
                Console.WriteLine(@"Canceled");
            }
            else
            {
                Console.WriteLine(@"Success");
            }

            lock (typeof(Program))
            {
                finished = true;
            }
        }
    }
}
