﻿using Discover.Solutions.Dell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    class Program
    {
        static void Main(string[] args)
        {
            IoC.Register<IDellDiscoverContext, DellDiscoverContext>();
            IoC.Register<IDellDiscoverService, DellDiscoverService>();
            IoC.Register<IDellProductEntityStore, DellProductEntityStore>();
            IoC.Register<IDellSerieEntityStore, DellSerieEntityStore>();
            IoC.Register<IDellWebLinkEntityStore, DellWebLinkEntityStore>();

            var dellDiscover = new DellDiscover();

            //dellDiscover.Start(DellWebFile.UsHome.Url());

            dellDiscover.Download(@"http://accessories.us.dell.com/", @"C:\temp");

        }
    }
}
