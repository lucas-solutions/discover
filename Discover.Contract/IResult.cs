﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public interface IResult
    {
        object Value { get; }

        ICollection<string> Errors { get; }

        bool Succeeded { get; }
    }

    public interface IResult<out TValue> : IResult
    {
        new TValue Value { get; }
    }
}
