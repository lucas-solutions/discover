﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public static class IoC
    {
        private static readonly SimpleInjector.Container _container;

        static IoC()
        {
            _container = new SimpleInjector.Container();
        }

        public static SimpleInjector.Container Container
        {
            get { return _container; }
        }

        /// <summary>
        ///     Gets an instance of the given <typeparamref name="TService" /> type.
        /// </summary>
        /// <typeparam name="TService">The type of the requested service.</typeparam>
        /// <returns>The requested service instance.</returns>
        public static TService GetInstance<TService>()
        {
            return (TService)_container.GetInstance(typeof(TService));
        }

        /// <summary>
        ///     Registers the <typeparamref name="TService" /> with the <typeparamref name="TImplementation" />.
        /// </summary>
        /// <typeparam name="TService">The service type to register.</typeparam>
        /// <typeparam name="TImplementation">The implementing type.</typeparam>
        public static void Register<TService, TImplementation>() where TImplementation : TService
        {
            _container.Register(typeof(TService), typeof(TImplementation));
        }
    }
}
