﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public interface IDellProductEntityStore : IEntityStore<DellProduct>
    {
        IResult<DellProduct> Save(DellProduct product);
    }
}
