﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public static class DellWebPageHelper
    {
        static string BusinessSegment = "business";

        static string CategoryParam = "category_id";

        static string CountryParam = "c";

        static string LanguageParam = "l";

        static string SectionParam = "s";

        static string[] TrackParams = new[] { "~ck", "link_number" };

        public static bool IsLanguageCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return false;

            if (code.Length != 2) return false;

            CultureInfo culture;

            try
            {
                culture = new CultureInfo(code);
            }
            catch
            {
                culture = null;
            }

            return culture != null;
        }

        public static bool IsCountryCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return false;

            if (code.Length != 2) return false;

            RegionInfo region;

            try
            {
                region = new RegionInfo(code);
            }
            catch
            {
                region = null;
            }

            return region != null;
        }
    }
}
