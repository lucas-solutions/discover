﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellWebLink
    {
        public virtual bool Ignore { get; set; }

        public virtual DellSerie Source { get; set; }

        public virtual int SourceId { get; set; }

        public virtual DellSerie Target { get; set; }

        public virtual int TargetId { get; set; }

        public virtual string Text { get; set; }

        public virtual string Url { get; set; }

        public virtual DateTime UrlUtc { get; set; }
    }
}
