﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public interface IDellSerieEntityStore : IEntityStore<DellSerie>
    {
        DellSerie FindByPath(DellSerie filter);

        IResult<DellSerie> Save(DellSerie file);
    }
}
