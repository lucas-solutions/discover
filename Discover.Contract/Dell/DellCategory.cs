﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellCategory
    {
        public static DellCategory MakerBolt3dPrinter = new DellCategory { Id = 4014, Name = "" };

        public int Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Scan priority
        /// </summary>
        public virtual byte Priority { get; set; }
    }
}
