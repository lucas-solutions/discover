﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellCountry
    {
        public static DellCountry CA = new DellCountry { Id = "ca", Name = "Canada" };

        public static DellCountry US = new DellCountry { Id = "us", Name = "United States" };

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
