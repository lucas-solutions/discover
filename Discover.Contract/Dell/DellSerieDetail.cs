﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellSerieDetail
    {
        public virtual string ContentUrl { get; set; }

        public virtual string Country { get; set; }

        public virtual string Currency { get; set; }

        public virtual DateTime DateUtc { get; set; }

        public virtual string ImageUrl { get; set; }

        public virtual string Language { get; set; }

        public virtual DellSerie Serie { get; set; }

        public virtual int SerieId { get; set; }

        public virtual decimal StartPrice { get; set; }
    }
}
