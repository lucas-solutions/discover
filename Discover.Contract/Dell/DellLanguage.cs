﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellLanguage
    {
        public static DellLanguage English = new DellLanguage { Id = "en", Name = "English" };

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
