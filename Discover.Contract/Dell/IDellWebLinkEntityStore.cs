﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public interface IDellWebLinkEntityStore : IEntityStore<DellWebLink>
    {
        IResult<DellWebLink> Save(DellWebLink link);
    }
}
