﻿using Discover.Solutions.Web.Spider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public interface IDellDiscoverContext
    {
        IDellProductEntityStore DellProductEntityStore { get; }

        IDellSerieEntityStore DellWebFileEntityStore { get; }

        IDellWebLinkEntityStore DellWebLinkEntityStore { get; }

        IWebSpiderEntryEntityStore WebSpiderEntryEntityStore { get; }

        IWebSpiderProjectEntityStore WebSpiderProjectEntityStore { get; }
    }
}
