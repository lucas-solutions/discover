﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellSerie
    {
        public virtual bool Business { get; set; }

        public virtual int? Category { get; set; }

        public virtual string ContentUrl { get; set; }

        public virtual DateTime DateUtc { get; set; }

        public virtual int Id { get; set; }

        public virtual string ImageUrl { get; set; }

        /// <summary>
        /// Scan priority
        /// </summary>
        public virtual byte Priority { get; set; }

        public virtual string Section { get; set; }

        public virtual string Summary { get; set; }

        public virtual string Title { get; set; }
    }
}
