﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellProductDetail
    {
        public virtual string ContentUrl { get; set; }

        public virtual string Country { get; set; }

        public virtual string Currency { get; set; }

        public virtual DateTime DateUtc { get; set; }

        public virtual string ImageUrl { get; set; }

        public virtual string Language { get; set; }

        public virtual decimal Price { get; set; }

        public virtual DellProduct Product { get; set; }

        public virtual int ProductId { get; set; }

        public virtual string Url { get; set; }

        
    }
}
