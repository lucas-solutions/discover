﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellProduct
    {
        private ICollection<DellProductDetail> _detail;

        public virtual int Category { get; set; }

        public virtual string CategoryPath { get; set; }

        public virtual string ContentUrl { get; set; }

        public virtual DateTime DateUtc { get; set; }

        public virtual string Description { get; set; }

        public virtual ICollection<DellProductDetail> Details
        {
            get { return _detail ?? (_detail = new Collection<DellProductDetail>()); }
            set { _detail = value; }
        }

        public virtual int Id { get; set; }

        public virtual string ImageUrl { get; set; }

        public virtual string Model { get; set; }

        /// <summary>
        /// Scan priority
        /// </summary>
        public virtual byte Priority { get; set; }

        public virtual string Segment { get; set; }

        public virtual DellSerie Serie { get; set; }

        /// <summary>
        /// Primary key for the product
        /// </summary>
        public virtual int? SerieId { get; set; }

        public virtual string Title { get; set; }

        public virtual string Url { get; set; }
    }
}
