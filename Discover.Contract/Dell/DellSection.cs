﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellSection
    {
        public static DellSection DesktopsAndWorkstations = new DellSection("desktops-n-workstations");

        public static DellSection EnterpriceProducts = new DellSection("enterprise-products");

        public static DellSection Laptops = new DellSection("laptops");

        public static DellSection TabletPCsMobileDevices = new DellSection("tablet-pcs-mobile-devices");

        public static DellSection DesktopAllInOneDeals = new DellSection("desktop-all-in-one-deals");

        public static DellSection Workstations = new DellSection("workstations");

        public DellSection()
        {
        }

        private DellSection(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        /// <summary>
        /// Scan priority
        /// </summary>
        public virtual byte Priority { get; set; }
    }
}
