﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public class Result : IResult
    {
        private class Success : IResult
        {
            object IResult.Value
            {
                get { return null; }
            }

            ICollection<string> IResult.Errors
            {
                get { return Array.AsReadOnly(new string[0]); }
            }

            bool IResult.Succeeded
            {
                get { return true; }
            }
        }

        private static readonly IResult _success = new Success();

        public static IResult Ok()
        {
            return _success;
        }

        public static IResult<TValue> Ok<TValue>(TValue value)
        {
            return new Result<TValue>(value);
        }

        public static IResult Fail<TError>(string message)
            where TError : Exception
        {
            var result = new Result();

            result.Errors.Add(message);

            return result;
        }

        public static IResult Fail(string format, params object[] args)
        {
            var result = new Result { _errors = Array.AsReadOnly(new [] { string.Format(format, args) }) };

            return result;
        }

        public static IResult Fail(params string[] errors)
        {
            var result = new Result { _errors = new ReadOnlyCollection<string>(errors) };

            return result;
        }

        public static IResult Fail(IList<string> errors)
        {
            var result = new Result { _errors = new ReadOnlyCollection<string>(errors) };

            return result;
        }

        public static IResult Fail(IEnumerable<string> errors)
        {
            var result = new Result { _errors = new ReadOnlyCollection<string>(errors.ToArray()) };

            return result;
        }

        public static IResult InvalidOperation(string message)
        {
            return Fail<InvalidOperationException>(message);
        }

        public static IResult InvalidOperation(string format, params object[] args)
        {
            return Fail(format, args);
        }

        protected ICollection<string> _errors;

        public object Value
        {
            get;
            set;
        }

        public ICollection<string> Errors
        {
            get { return _errors ?? (_errors = new Collection<string>()); }
        }

        public bool HasErrors
        {
            get { return _errors != null && _errors.Count > 0; }
        }

        public bool Succeeded
        {
            get { return !HasErrors; }
        }
    }

    public class Result<TValue> : Result, IResult<TValue>
    {
        public static IResult<TValue> Ok(TValue value)
        {
            return new Result<TValue>(value);
        }

        new public static IResult<TValue> Fail(params string[] errors)
        {
            var result = new Result<TValue> { _errors = new ReadOnlyCollection<string>(errors) };

            return result;
        }

        new public static IResult<TValue> Fail(IList<string> errors)
        {
            var result = new Result<TValue> { _errors = new ReadOnlyCollection<string>(errors) };

            return result;
        }

        new public static IResult<TValue> Fail(IEnumerable<string> errors)
        {
            var result = new Result<TValue> { _errors = new ReadOnlyCollection<string>(errors.ToArray()) };

            return result;
        }

        new public static IResult<TValue> Fail<TError>(string format, params object[] args)
            where TError : Exception
        {
            var result = new Result<TValue>();

            result.Errors.Add(string.Format(format, args));

            return result;
        }

        public Result()
        {
        }

        public Result(TValue value)
        {
            Value = value;
        }

        new public TValue Value { get; private set; }
    }
}
