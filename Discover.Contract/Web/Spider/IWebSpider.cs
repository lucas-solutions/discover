﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public interface IWebSpider
    {
        WebSpiderCollection Collection { get; }

        WebSpiderEntryQueue Queue { get; }

        IResult Download(ref bool stop);
    }
}
