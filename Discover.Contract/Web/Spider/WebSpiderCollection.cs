﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderCollection : KeyedCollection<Uri, WebSpiderEntry>
    {
        protected override Uri GetKeyForItem(WebSpiderEntry item)
        {
            return new Uri(item.RemoteUrl, UriKind.Absolute);
        }

        public WebSpiderEntry SingleOrDefault(Uri absoluteUri)
        {
            WebSpiderEntry value;

            if (base.Dictionary != null && base.Dictionary.TryGetValue(absoluteUri, out value)) return value;

            if (base.Contains(absoluteUri)) return base[absoluteUri];

            return null;
        }
    }
}
