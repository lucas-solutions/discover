﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public enum WebSpiderEntryState
    {
        Added = 0,

        Enqueued = 1,

        Success = 2,

        Fail = 4,
    }
}
