﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderError
    {
        public virtual string Error { get; set; }

        public virtual int ErrorCode { get; set; }
        
        public virtual DateTime ErrorUtc { get; set; }

        public virtual WebSpiderProject Project { get; set; }

        public virtual int ProjectId { get; set; }

        public virtual WebSpiderEntry Entry { get; set; }

        public virtual long? EntryId { get; set; }
    }
}
