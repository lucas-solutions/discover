﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    [Serializable]
    public class WebSpiderProject
    {
        [NonSerialized]
        private ICollection<WebSpiderError> _errors;
        private ICollection<WebSpiderEntry> _entries;
        private int? _refreshQueueInterval;
        private int? _saveCollectionInterval;

        public string BaseUrl { get; set; }
        
        public virtual ICollection<WebSpiderError> Errors
        {
            get { return _errors ?? (_errors = new Collection<WebSpiderError>()); }
            set { _errors = value; }
        }

        public virtual DownloadProxyUsage ProxyUsage { get; set; }

        public virtual string Proxy { get; set; }

        public virtual ICollection<WebSpiderEntry> Entries
        {
            get { return _entries ?? (_entries = new Collection<WebSpiderEntry>()); }
            set { _entries = value; }
        }

        public virtual bool StayOnSite { get; set; }

        public virtual int Id { get; set; }

        public virtual byte MaximumEntryLevel { get; set; }

        public virtual int RefreshQueueInterval
        {
            get { return _refreshQueueInterval ?? 100; }
            set { _refreshQueueInterval = value; }
        }

        public virtual int SaveCollectionInterval
        {
            get { return _saveCollectionInterval ?? 100; }
            set { _saveCollectionInterval = value; }
        }
    }
}
