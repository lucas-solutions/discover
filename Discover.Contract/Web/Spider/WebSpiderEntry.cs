﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    [Serializable]
    public class WebSpiderEntry
    {
        [NonSerialized]
        private ICollection<WebSpiderError> _errors;

        public virtual ICollection<WebSpiderError> Errors
        {
            get { return _errors ?? (_errors = new Collection<WebSpiderError>()); }
            set { _errors = value; }
        }

        /// <summary>
        /// External Link
        /// </summary>
        public virtual bool External { get; set; }

        public virtual long Id { get; set; }

        public virtual byte Level { get; set; }

        public virtual string LocalPath { get; set; }

        public virtual string RemoteUrl { get; set; }

        public virtual string ContentDisposition { get; set; }

        public virtual string ContentType { get; set; }

        public virtual bool Download { get; set; }

        public virtual HttpStatusCode DownloadStatus { get; set; }

        public virtual DateTime? DownloadUtc { get; set; }

        public virtual bool Page { get; set; }

        public virtual byte Priority { get; set; }

        public virtual WebSpiderProject Project { get; set; }

        public virtual int ProjectId { get; set; }

        public virtual UriType ResourceType { get; set; }

        public virtual WebSpiderEntryState State { get; set; }

        public virtual DateTime? SuccessUtc { get; set; }

        /// <summary>
        /// Indicates whether the current object is equal to another object of 
        /// the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the other parameter; 
        /// otherwise, false.
        /// </returns>
        public bool Equals(WebSpiderEntry other)
        {
            if (RemoteUrl == null && other.RemoteUrl == null) return true;

            if (RemoteUrl == null || other.RemoteUrl == null) return false;

            int result = string.Compare(RemoteUrl, other.RemoteUrl, true);

            return result == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (base.Equals(obj)) return true;

            return obj is WebSpiderEntry ? Equals(obj as WebSpiderEntry) : false;
        }
    }
}
