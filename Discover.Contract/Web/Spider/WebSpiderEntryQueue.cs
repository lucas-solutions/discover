﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderEntryQueue : Queue<WebSpiderEntry>
    {
        public WebSpiderEntryQueue()
        {
        }

        public WebSpiderEntryQueue(IEnumerable<WebSpiderEntry> collection)
            : base(collection)
        {
        }
    }
}
