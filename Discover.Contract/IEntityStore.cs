﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public interface IEntityStore : IDisposable
    {
        bool AutoSaveChanges { get; set; }
    }

    public interface IEntityStore<TEntity> : IEntityStore
        where TEntity : class
    {
        // Ensures the entity is connected to the store
        TEntity Entry(TEntity entity);

        TEntity Detach(TEntity entity);

        TEntity FindById(TEntity filter);

        TEntity FindById(params object[] id);

        Task<TEntity> FindByIdAsync(params object[] id);

        TEntity First(Expression<Func<TEntity, bool>> filter);

        Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// Create a new entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        IResult<TEntity> Create(TEntity entity);

        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Delete(TEntity entity);

        /// <summary>
        /// Delete matching entities
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        int Delete(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Update(TEntity entity);
        IQueryable<TEntity> EntitySet { get; }

        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter);

        void Load(bool eager);

        IResult SaveChanges();
    }
}
