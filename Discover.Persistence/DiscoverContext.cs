﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public class DiscoverContext
    {
        public DiscoverContext()
            : this(new DiscoverDbContext())
        {
        }

        internal DiscoverContext(DiscoverDbContext dbContext)
        {
            DbContext = dbContext;
        }

        internal DiscoverDbContext DbContext { get; private set; }


    }
}
