﻿using System;
using System.Data.Entity;
using Discover.Solutions.Dell;
using Discover.Solutions.Web.Spider;

namespace Discover.Solutions
{
    [DbConfigurationType("Discover.Solutions.DiscoverDbConfiguration, Discover.Persistence")] 
    internal class DiscoverDbContext : DbContext
    {
        public DiscoverDbContext()
            : base("DefaultConnection")
        {
        }

        public DiscoverDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder");
            }

            // Dell
            modelBuilder.Configurations
                //.Add(new DellCountryEntityConfiguration())
                //.Add(new DellLanguageEntityConfiguration())
                .Add(new DellProductEntityConfiguration())
                .Add(new DellProductDetailEntityConfiguration())
                //.Add(new DellCategoryEntityConfiguration())
                .Add(new DellSerieEntityConfiguration())
                .Add(new DellSerieDetailEntityConfiguration())
                .Add(new DellWebLinkEntityConfiguration())
                .Add(new WebSpiderProjectEntityConfiguration())
                .Add(new WebSpiderEntryEntityConfiguration())
                .Add(new WebSpiderErrorEntityConfiguration());
        }
    }
}