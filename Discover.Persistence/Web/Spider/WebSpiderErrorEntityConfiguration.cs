﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    internal class WebSpiderErrorEntityConfiguration : EntityTypeConfiguration<WebSpiderError>
    {
        public WebSpiderErrorEntityConfiguration()
        {
            ToTable("[WEBS].Errors");

            HasKey(error => new { error.ProjectId, error.ErrorUtc });

            HasRequired(error => error.Project)
                .WithMany(project => project.Errors)
                .HasForeignKey(error => error.ProjectId)
                .WillCascadeOnDelete(true);

            HasOptional(error => error.Entry)
                .WithMany(entry => entry.Errors)
                .HasForeignKey(error => error.EntryId)
                .WillCascadeOnDelete(true);

            Property(error => error.EntryId)
                .HasColumnName("Entry");

            Property(error => error.ProjectId)
                .HasColumnName("Project");

            Property(error => error.ErrorUtc)
                .HasColumnType("datetime2");
        }
    }
}
