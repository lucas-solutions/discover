﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderProjectEntityStore : EntityStore<WebSpiderProject>, IWebSpiderProjectEntityStore
    {
        public WebSpiderProjectEntityStore()
            : base(new DiscoverDbContext())
        {
        }

        internal WebSpiderProjectEntityStore(DiscoverContext context)
            : base(context.DbContext)
        {
        }

        public override WebSpiderProject FindById(WebSpiderProject filter)
        {
            return base.FindById(filter.Id);
        }

        public override Task<WebSpiderProject> FindByIdAsync(WebSpiderProject filter)
        {
            return base.FindByIdAsync(filter.Id);
        }

        public WebSpiderProject FindByUrl(string baseUrl)
        {
            return base.First(entry => entry.BaseUrl == baseUrl);
        }
    }
}
