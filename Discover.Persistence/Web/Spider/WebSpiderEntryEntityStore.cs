﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderEntryEntityStore : EntityStore<WebSpiderEntry>, IWebSpiderEntryEntityStore
    {
        public WebSpiderEntryEntityStore()
            : base(new DiscoverDbContext())
        {
        }

        public WebSpiderEntryEntityStore(DiscoverContext context)
            : base(context.DbContext)
        {
        }

        public override WebSpiderEntry FindById(WebSpiderEntry filter)
        {
            return base.FindById(filter.Id);
        }

        public override Task<WebSpiderEntry> FindByIdAsync(WebSpiderEntry filter)
        {
            return base.FindByIdAsync(filter.Id);
        }

        public virtual IResult<WebSpiderEntry> Save(WebSpiderEntry entry)
        {
            WebSpiderEntry entity = null;
            ICollection<string> errors = null;

            try
            {
                entity = Entry(entry);

                if (entity != null)
                {
                    entity.Download = entry.Download;
                    entity.DownloadUtc = entry.DownloadUtc;
                    if (entry.SuccessUtc.HasValue) entity.SuccessUtc = entry.SuccessUtc;
                    errors = SaveChanges().Errors;
                }
                else
                {
                    var opCreate = base.Create(entry);

                    if (opCreate.Succeeded) entity = opCreate.Value;
                    else errors = opCreate.Errors;
                }
            }
            catch (Exception e)
            {
                errors = new[] { e.ToString() };
            }

            return entity != null ? Result<WebSpiderEntry>.Ok(entity) : Result<WebSpiderEntry>.Fail(errors);
        }
    }
}
