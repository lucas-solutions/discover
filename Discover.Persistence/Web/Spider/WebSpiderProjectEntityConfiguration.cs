﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    internal class WebSpiderProjectEntityConfiguration : EntityTypeConfiguration<WebSpiderProject>
    {
        public WebSpiderProjectEntityConfiguration()
        {
            ToTable("[WEBS].Projects");

            HasKey(project => project.Id);

            HasMany(project => project.Errors)
                .WithRequired(error => error.Project)
                .HasForeignKey(error => error.ProjectId)
                .WillCascadeOnDelete(true);

            HasMany(project => project.Entries)
                .WithRequired(resource => resource.Project)
                .HasForeignKey(resource => resource.ProjectId)
                .WillCascadeOnDelete(true);

            Property(project => project.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(project => project.BaseUrl)
                .HasColumnName("Url")
                .HasMaxLength(255)
                .IsUnicode(false)
                .IsVariableLength();
        }
    }
}
