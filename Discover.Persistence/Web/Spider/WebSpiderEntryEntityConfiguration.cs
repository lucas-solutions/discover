﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    internal class WebSpiderEntryEntityConfiguration : EntityTypeConfiguration<WebSpiderEntry>
    {
        public WebSpiderEntryEntityConfiguration()
        {
            ToTable("[WEBS].Entries");

            HasKey(entry => entry.Id);

            HasMany(resource => resource.Errors)
                .WithOptional(error => error.Entry)
                .HasForeignKey(error => error.EntryId)
                .WillCascadeOnDelete(true);

            HasRequired(resource => resource.Project)
                .WithMany(project => project.Entries)
                .HasForeignKey(resource => resource.ProjectId)
                .WillCascadeOnDelete(true);

            Property(entry => entry.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(entry => entry.DownloadUtc)
                .HasColumnType("datetime2")
                .IsOptional();

            Property(entry => entry.LocalPath)
                .HasColumnName("Local")
                .HasMaxLength(2000)
                .IsUnicode(false)
                .IsVariableLength();

            Property(entry => entry.RemoteUrl)
                .HasColumnName("Remote")
                .HasMaxLength(1000)
                .IsUnicode(false)
                .IsVariableLength();

            Property(entry => entry.SuccessUtc)
                .HasColumnType("datetime2")
                .IsOptional();
        }
    }
}
