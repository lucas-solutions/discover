﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    internal class DiscoverDbConfiguration : DbConfiguration 
    { 
        public DiscoverDbConfiguration() 
        { 
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy()); 
            SetDefaultConnectionFactory(new LocalDbConnectionFactory("v11.0")); 
        } 
    }
}
