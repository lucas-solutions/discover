﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellSerieEntityStore : EntityStore<DellSerie>, IDellSerieEntityStore
    {
        public DellSerieEntityStore()
            : base(new DiscoverDbContext())
        {
        }

        public DellSerieEntityStore(DiscoverContext context)
            : base(context.DbContext)
        {
        }

        public override DellSerie FindById(DellSerie filter)
        {
            return base.FindById(filter.Id);
        }

        public override Task<DellSerie> FindByIdAsync(DellSerie filter)
        {
            return base.FindByIdAsync(filter.Id);
        }

        public virtual DellSerie FindByPath(DellSerie filter)
        {
            return base.EntityDbSet.FirstOrDefault(serie => serie.ContentUrl == filter.ContentUrl);
        }

        public virtual IResult<DellSerie> Save(DellSerie file)
        {
            DellSerie entry = null;
            ICollection<string> errors = null;

            try
            {
                entry = Entry(file) ?? FindByPath(file);

                if (entry == null)
                {
                    var opCreate = base.Create(file);

                    if (opCreate.Succeeded) entry = opCreate.Value;
                    else errors = opCreate.Errors;
                }
                else if (entry != file)
                {
                    entry.DateUtc = file.DateUtc;
                    entry.Title = file.Title;
                    entry.Summary = file.Summary;
                    entry.Section = file.Section;
                }

                errors = SaveChanges().Errors;
            }
            catch (Exception e)
            {
                errors = new[] { e.ToString() };
            }

            return entry != null ? Result<DellSerie>.Ok(entry) : Result<DellSerie>.Fail(string.Join(". ", errors));
        }
    }
}
