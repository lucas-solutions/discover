﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellSerieDetailEntityConfiguration : EntityTypeConfiguration<DellSerieDetail>
    {
        public DellSerieDetailEntityConfiguration()
        {
            ToTable("[DELL].HSeries");

            HasKey(detail => new { detail.SerieId, detail.DateUtc });

            HasRequired(detail => detail.Serie)
                .WithMany()
                .HasForeignKey(detail => detail.SerieId)
                .WillCascadeOnDelete(true);


            Property(detail => detail.DateUtc)
                .HasColumnType("datetime2");

            Property(detail => detail.Country)
                .HasMaxLength(2)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);

            Property(detail => detail.Currency)
                .HasMaxLength(3)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(detail => detail.Language)
                .HasMaxLength(2)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);

            Property(detail => detail.ImageUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(detail => detail.ContentUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();
        }
    }
}
