﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellWebLinkEntityConfiguration : EntityTypeConfiguration<DellWebLink>
    {
        public DellWebLinkEntityConfiguration()
        {
            ToTable("[DELL].Links");

            HasKey(link => new { link.SourceId, link.TargetId });

            HasRequired(link => link.Source)
                .WithMany()
                .HasForeignKey(link => link.SourceId)
                .WillCascadeOnDelete(false);

            HasRequired(link => link.Target)
                .WithMany()
                .HasForeignKey(link => link.TargetId)
                .WillCascadeOnDelete(false);

            Property(link => link.Text)
                .HasMaxLength(1000)
                .IsRequired()
                .IsUnicode(false)
                .IsVariableLength();

            Property(link => link.Url)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(false)
                .IsVariableLength();

            Property(link => link.UrlUtc)
                .IsOptional()
                .HasColumnType("datetime2");
        }
    }
}
