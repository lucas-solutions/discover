﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellCountryEntityConfiguration : EntityTypeConfiguration<DellCountry>
    {
        public DellCountryEntityConfiguration()
        {
            ToTable("[DELL].Countries");

            HasKey(country => country.Id);

            Property(country => country.Id)
                .IsUnicode(false)
                .IsFixedLength()
                .HasMaxLength(2);

            Property(country => country.Name)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength()
                .HasMaxLength(100);
        }
    }
}
