﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellSerieEntityConfiguration : EntityTypeConfiguration<DellSerie>
    {
        public DellSerieEntityConfiguration()
        {
            ToTable("[DELL].Series");

            HasKey(serie => serie.Id);

            Property(serie => serie.DateUtc)
                .HasColumnType("datetime2");

            Property(serie => serie.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(serie => serie.ContentUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(serie => serie.ImageUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(serie => serie.Section)
                .HasMaxLength(10)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(serie => serie.Summary)
                .HasMaxLength(2000)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);

            Property(serie => serie.Title)
                .HasMaxLength(200)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);
        }
    }
}
