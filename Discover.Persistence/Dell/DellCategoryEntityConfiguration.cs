﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellCategoryEntityConfiguration : EntityTypeConfiguration<DellCategory>
    {
        public DellCategoryEntityConfiguration()
        {
            ToTable("[DELL].Categories");

            HasKey(category => category.Id);

            Property(category => category.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(category => category.Name)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength()
                .HasMaxLength(100);
        }
    }
}
