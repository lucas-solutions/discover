﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellProductDetailEntityConfiguration : EntityTypeConfiguration<DellProductDetail>
    {
        public DellProductDetailEntityConfiguration()
        {
            ToTable("[DELL].HProducts");

            HasKey(detail => new { detail.ProductId, detail.DateUtc });

            Property(detail => detail.DateUtc)
                .HasColumnType("datetime2");

            Property(detail => detail.ContentUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(detail => detail.Country)
                .HasMaxLength(2)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);

            Property(detail => detail.Currency)
                .HasMaxLength(3)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(detail => detail.ImageUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(detail => detail.Language)
                .HasMaxLength(2)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);
        }
    }
}
