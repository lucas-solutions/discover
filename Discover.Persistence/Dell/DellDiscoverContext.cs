﻿using Discover.Solutions.Web.Spider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellDiscoverContext : DiscoverContext, IDellDiscoverContext
    {
        private DellProductEntityStore _dellProductEntityStore;
        private DellSerieEntityStore _dellWebFileEntityStore;
        private DellWebLinkEntityStore _dellWebLinkEntityStore;
        private WebSpiderEntryEntityStore _webSpiderEntryEntityStore;
        private WebSpiderProjectEntityStore _webSpiderProjectEntityStore;

        public DellDiscoverContext()
            : base()
        {
        }

        /*public DellDiscoverContext(DiscoverContext context)
            : base(context.DbContext)
        {
        }*/

        internal DellDiscoverContext(DiscoverDbContext dbContext)
            : base(dbContext)
        {
        }
        
        public IDellSerieEntityStore DellWebFileEntityStore { get { return _dellWebFileEntityStore ?? (_dellWebFileEntityStore = new DellSerieEntityStore(this)); } }

        public IDellWebLinkEntityStore DellWebLinkEntityStore { get { return _dellWebLinkEntityStore ?? (_dellWebLinkEntityStore = new DellWebLinkEntityStore(this)); } }

        public IDellProductEntityStore DellProductEntityStore { get {return _dellProductEntityStore ?? (_dellProductEntityStore = new DellProductEntityStore(this)); } }

        public IWebSpiderEntryEntityStore WebSpiderEntryEntityStore { get { return _webSpiderEntryEntityStore ?? (_webSpiderEntryEntityStore = new WebSpiderEntryEntityStore(this)); } }

        public IWebSpiderProjectEntityStore WebSpiderProjectEntityStore { get { return _webSpiderProjectEntityStore ?? (_webSpiderProjectEntityStore = new WebSpiderProjectEntityStore(this)); } }

    }
}
