﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellLanguageEntityConfiguration : EntityTypeConfiguration<DellLanguage>
    {
        public DellLanguageEntityConfiguration()
        {
            ToTable("[DELL].Languages");

            HasKey(language => language.Id);

            Property(language => language.Id)
                .IsUnicode(false)
                .IsFixedLength()
                .HasMaxLength(2);

            Property(language => language.Name)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength()
                .HasMaxLength(100);
        }
    }
}
