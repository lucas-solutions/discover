﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellProductEntityStore : EntityStore<DellProduct>, IDellProductEntityStore
    {
        public DellProductEntityStore()
            : base(new DiscoverDbContext())
        {
        }

        public DellProductEntityStore(DiscoverContext context)
            : base(context.DbContext)
        {
        }

        public override DellProduct FindById(DellProduct filter)
        {
            return base.FindById(filter.Id);
        }

        public override Task<DellProduct> FindByIdAsync(DellProduct filter)
        {
            return base.FindByIdAsync(filter.Id);
        }

        public virtual IResult<DellProduct> Save(DellProduct product)
        {
            DellProduct entity = null;
            ICollection<string> errors = null;

            try
            {
                entity = Entry(product);

                if (entity != null)
                {
                    //entity.VisitUtc = product.VisitUtc;
                    errors = SaveChanges().Errors;
                }
                else
                {
                    var opCreate = base.Create(product);

                    if (opCreate.Succeeded) entity = opCreate.Value;
                    else errors = opCreate.Errors;
                }
            }
            catch (Exception e)
            {
                errors = new[] { e.ToString() };
            }

            return entity != null ? Result<DellProduct>.Ok(entity) : Result<DellProduct>.Fail(errors);
        }
    }
}
