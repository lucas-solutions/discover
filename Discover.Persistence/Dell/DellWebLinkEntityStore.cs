﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellWebLinkEntityStore : EntityStore<DellWebLink>, IDellWebLinkEntityStore
    {
        public DellWebLinkEntityStore()
            : base(new DiscoverDbContext())
        {
        }

        public DellWebLinkEntityStore(DiscoverContext context)
            : base(context.DbContext)
        {
        }

        public override DellWebLink FindById(DellWebLink filter)
        {
            return base.FindById(filter.SourceId, filter.TargetId);
        }

        public override Task<DellWebLink> FindByIdAsync(DellWebLink filter)
        {
            return base.FindByIdAsync(filter.SourceId, filter.TargetId);
        }

        public virtual IResult<DellWebLink> Save(DellWebLink link)
        {
            DellWebLink entry = null;
            ICollection<string> errors = null;

            try
            {
                entry = Entry(link);

                if (entry == null)
                {
                    var opCreate = base.Create(link);

                    if (opCreate.Succeeded) entry = opCreate.Value;
                    else errors = opCreate.Errors;
                }
                else if (entry != link)
                {
                    entry.Ignore = link.Ignore;
                    entry.UrlUtc = link.UrlUtc;
                }

                errors = SaveChanges().Errors;
            }
            catch (Exception e)
            {
                errors = new[] { e.ToString() };
            }

            return entry != null ? Result<DellWebLink>.Ok(entry) : Result<DellWebLink>.Fail(string.Join(". ", errors));
        }
    }
}
