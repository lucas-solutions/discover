﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    internal class DellProductEntityConfiguration : EntityTypeConfiguration<DellProduct>
    {
        public DellProductEntityConfiguration()
        {
            ToTable("[DELL].Products");

            HasKey(product => product.Id);

            Property(product => product.DateUtc)
                .HasColumnType("datetime2");

            Property(product => product.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(product => product.ContentUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(product => product.ImageUrl)
                .HasMaxLength(1000)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(product => product.Segment)
                .HasMaxLength(10)
                .IsOptional()
                .IsUnicode(false)
                .IsVariableLength();

            Property(product => product.Description)
                .HasMaxLength(2000)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);

            Property(product => product.Title)
                .HasMaxLength(200)
                .IsFixedLength()
                .IsOptional()
                .IsUnicode(false);
        }
    }
}
