﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Discover.Solutions
{
    public abstract class EntityStore<TEntity> : IEntityStore<TEntity>
        where TEntity : class
    {
        protected bool Disposed { get; private set; }

        protected EntityStore(DbContext dbContext)
        {
            AutoSaveChanges = true;

            TraceChances = true;

            DbContext = dbContext;

            EntityDbSet = dbContext.Set<TEntity>();
        }

        internal IObjectContextAdapter Adapter { get { return DbContext; } }

        public bool TraceChances { get; set; }

        internal DbContext DbContext { get; private set; }

        public bool DisposeContext { get; set; }

        protected DbSet<TEntity> EntityDbSet { get; private set; }

        public bool AutoSaveChanges { get; set; }

        public virtual IQueryable<TEntity> EntitySet
        {
            get { return EntityDbSet; }
        }

        public virtual TEntity Entry(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            if (entity == null) return null;

            var entry = DbContext.Entry(entity);

            if (entry != null && entry.State != EntityState.Detached)
            {
                entry.Reload();
                entity = entry.Entity;
            }
            else
            {
                entity = FindById(entity);
                if (entity != null && DbContext.Entry(entity).State == EntityState.Detached) entity = null;
            }

            return entity;
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            return EntityDbSet.Where(filter);
        }

        public virtual IResult<TEntity> Create(TEntity model)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            TEntity entity = EntityDbSet.Add(model);

            var opSaveChanges = SaveChanges();

            if (!opSaveChanges.Succeeded)
            {
                return Result<TEntity>.Fail(opSaveChanges.Errors);
            }

            if (TraceChances) TraceChange(model);

            return Result<TEntity>.Ok(entity);
        }

        public virtual TEntity Delete(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            TEntity result = EntityDbSet.Remove(entity);

            SaveChanges();

            if (TraceChances) TraceChange(entity);

            return result;
        }

        /// <summary>
        /// Delete matching entities
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int Delete(Expression<Func<TEntity, bool>> filter)
        {
            var entities = EntityDbSet.Where(filter).ToList();

            EntityDbSet.RemoveRange(entities);

            SaveChanges();

            return entities.Count;
        }

        public virtual TEntity Detach(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            if (entity == null) return null;

            var context = Adapter.ObjectContext;

            context.ContextOptions.LazyLoadingEnabled = false;

            var entry = DbContext.Entry(entity);

            if (entry != null)
            {
                entry.Reload();
                entity = entry.Entity;
            }
            else
            {
                entity = FindById(entity);
            }

            context.Detach(entity);

            context.ContextOptions.LazyLoadingEnabled = true;

            return entity;
        }

        TEntity IEntityStore<TEntity>.FindById(TEntity filter)
        {
            return FindById(filter);
        }

        public virtual TEntity FindById(params object[] id)
        {
            return EntityDbSet.Find(id);
        }

        public virtual Task<TEntity> FindByIdAsync(params object[] id)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            return EntityDbSet.FindAsync(id);
        }

        public virtual void Load(bool eager)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            if (eager)
            {
                var context = Adapter.ObjectContext;

                context.ContextOptions.LazyLoadingEnabled = false;

                EntityDbSet.Load();

                context.ContextOptions.LazyLoadingEnabled = true;
            }
            else
            {
                EntityDbSet.Load();
            }
        }

        public virtual TEntity Update(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            if (entity != null)
            {
                DbContext.Entry(entity).State = EntityState.Modified;
            }

            if (TraceChances) TraceChange(entity);

            return entity;
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> filter)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            return EntitySet.FirstOrDefault(filter);
        }

        public virtual Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> filter)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            return EntitySet.FirstOrDefaultAsync(filter);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            TEntity result = EntityDbSet.Add(entity);

            await SaveChangesAsync();

            if (TraceChances) TraceChange(entity);

            return result;
        }

        public virtual async Task<TEntity> DeleteAsync(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            TEntity original = EntityDbSet.Remove(entity);

            var result = await SaveChangesAsync();

            if (TraceChances) TraceChange(entity);

            return original;
        }

        public abstract TEntity FindById(TEntity filter);

        public abstract Task<TEntity> FindByIdAsync(TEntity filter);

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            //Condition.ObjectNotDisposed(Disposed, e => { throw Trace.Error(e).Exception; });

            if (entity != null)
            {
                DbContext.Entry(entity).State = EntityState.Modified;
            }

            if (TraceChances) TraceChange(entity);

            return entity;
        }

        protected virtual void Dispose(bool disposing)
        {
            if ((DisposeContext && disposing) && (DbContext != null))
            {
                DbContext.Dispose();
            }
            Disposed = true;

            DbContext = null;

            EntityDbSet = null;
        }

        public IResult SaveChanges()
        {
            if (!AutoSaveChanges) return Result.Ok();

            IResult result = new Result();

            try
            {
                DbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var errors = new List<string>() { e.ToString() };
                
                //var error = new DbEntityValidationError();

                //result.Errors.Add(error.Issue(e));

                //Trace.Error<InvalidOperationException>(result.Errors);
                result = Result.Fail(errors);
            }
            catch (DbUpdateException e)
            {
                result = Result.Fail(e.ToString());
                Exception origen = e;
                for (; origen.InnerException != null; origen = e.InnerException) ;

                foreach (var entry in e.Entries)
                {
                    /*var message = new ErrorMessage<InvalidOperationError>
                    {
                        Info = new Dictionary<string, object>
                        {
                            {"State", entry.State},
                            {"Values", entry.CurrentValues.ToString() },
                            {"Source", e.Source}
                        },
                        Text = origen.Message,
                    };

                    if (entry.State != EntityState.Added)
                    {
                        message.Info.Add("Original", entry.OriginalValues.ToString());
                    }

                    result.Errors.Add(message);*/
                }

                //Trace.Error<InvalidOperationException>(result.Errors);
            }
            catch (SqlException e)
            {
                result = Result.Fail(e.ToString());
                /*var message = new ErrorMessage<InvalidOperationError>
                {
                    Info = new Dictionary<string, object>
                    {
                        {"Class", e.Class},
                        {"LineNumber", e.LineNumber},
                        {"Message", e.Message},
                        {"Number", e.Number},
                        {"Procedure", e.Procedure},
                        {"Server", e.Server},
                        {"Source", e.Source},
                        {"State", e.State}
                        //{ "ErrorCode", e.Win32ErrorCode }
                    },
                    Text = e.Message,
                };

                result.Errors.Add(message);

                Trace.Error<InvalidOperationException>(result.Errors);*/
            }
            catch (Exception e)
            {
                result = Result.Fail(e.ToString());
                //throw Trace.Error(e).Send().Exception;
            }

            return result;
        }

        protected async Task<IResult> SaveChangesAsync()
        {
            if (!AutoSaveChanges) return Result.Ok();

            var result = new Result();

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                //var error = new DbEntityValidationError();

                //result.Errors.Add(error.Issue(e));

                //Trace.Error<InvalidOperationException>(result.Errors);
            }
            /*catch (Exception e)
            {
                throw Trace.Error(e).Send().Exception;
            }*/

            return result;
        }

        // http://blogs.msdn.com/b/adonet/archive/2011/01/30/using-dbcontext-in-ef-feature-ctp5-part-5-working-with-property-values.aspx
        protected virtual void TraceChange(TEntity entity)
        {
            /*string timeStamp = DateTime.UtcNow.ToString("yyyy-M-d HH:mm:ss.ffff");
            string typeName = typeof(TEntity).Name;
            DbEntityEntry<TEntity> entry = DbContext.Entry(entity);
            object entityKey = DbContext.GetEntityKeyValue(entity);
            DbPropertyValues current;

            Dictionary<string, object> data;

            switch (entry.State)
            {
                case EntityState.Added:
                    current = entry.CurrentValues;
                    foreach (string name in current.PropertyNames)
                    {
                        object value = current[name];

                        data = new Dictionary<string, object>
                        {
                            {"Type", typeName},
                            {"Key", entityKey},
                            {"Proverty", name},
                            {"Value", value},
                            {"State", "Added"},
                            {"TimeStamp", timeStamp}
                        };

                        Trace.Info("Change", data);
                    }

                    break;

                case EntityState.Deleted:
                    data = new Dictionary<string, object>
                    {
                        {"Type", typeName},
                        {"Key", entityKey},
                        {"State", "Deleted"},
                        {"TimeStamp", timeStamp}
                    };

                    Trace.Info("Change", data);

                    break;

                case EntityState.Modified:
                    current = entry.CurrentValues;
                    foreach (string name in current.PropertyNames)
                    {
                        if (entry.Property(name).IsModified)
                        {
                            object value = current[name];

                            data = new Dictionary<string, object>
                            {
                                {"Type", typeName},
                                {"Key", entityKey},
                                {"Proverty", name},
                                {"Value", value},
                                {"State", "Modified"},
                                {"TimeStamp", timeStamp}
                            };

                            Trace.Info("Change", data);
                        }
                    }

                    break;
            }*/
        }
    }
}