namespace Discover.Solutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Two : DbMigration
    {
        public override void Up()
        {
            AlterColumn("DELL.Files", "Type", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("DELL.Files", "Type", c => c.String(maxLength: 10, unicode: false));
        }
    }
}
