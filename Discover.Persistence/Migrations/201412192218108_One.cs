namespace Discover.Solutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class One : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "DELL.Downloads",
                c => new
                    {
                        FileId = c.Int(nullable: false),
                        CreateUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Name = c.String(nullable: false, maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => new { t.FileId, t.CreateUtc })
                .ForeignKey("DELL.Files", t => t.FileId)
                .Index(t => t.FileId);
            
            CreateTable(
                "DELL.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Archive = c.String(nullable: false, maxLength: 255, unicode: false),
                        ArchiveUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Business = c.Boolean(nullable: false),
                        Category = c.Int(),
                        Country = c.String(maxLength: 2, fixedLength: true, unicode: false),
                        Done = c.Boolean(nullable: false),
                        DoneUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        Host = c.String(maxLength: 50, unicode: false),
                        Language = c.String(maxLength: 2, fixedLength: true, unicode: false),
                        Level = c.Byte(nullable: false),
                        Page = c.Boolean(nullable: false),
                        Path = c.String(maxLength: 100, unicode: false),
                        Priority = c.Byte(nullable: false),
                        ProductId = c.Int(),
                        Query = c.String(maxLength: 100, unicode: false),
                        Section = c.String(maxLength: 10, unicode: false),
                        Track = c.String(maxLength: 100, unicode: false),
                        Type = c.String(maxLength: 10, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("DELL.Products", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "DELL.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "DELL.Links",
                c => new
                    {
                        SourceId = c.Int(nullable: false),
                        TargetId = c.Int(nullable: false),
                        Ignore = c.Boolean(nullable: false),
                        Text = c.String(nullable: false, maxLength: 255, unicode: false),
                        Url = c.String(nullable: false, maxLength: 255, unicode: false),
                        UrlUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.SourceId, t.TargetId })
                .ForeignKey("DELL.Files", t => t.SourceId)
                .ForeignKey("DELL.Files", t => t.TargetId)
                .Index(t => t.SourceId)
                .Index(t => t.TargetId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("DELL.Links", "TargetId", "DELL.Files");
            DropForeignKey("DELL.Links", "SourceId", "DELL.Files");
            DropForeignKey("DELL.Downloads", "FileId", "DELL.Files");
            DropForeignKey("DELL.Files", "ProductId", "DELL.Products");
            DropIndex("DELL.Links", new[] { "TargetId" });
            DropIndex("DELL.Links", new[] { "SourceId" });
            DropIndex("DELL.Files", new[] { "ProductId" });
            DropIndex("DELL.Downloads", new[] { "FileId" });
            DropTable("DELL.Links");
            DropTable("DELL.Products");
            DropTable("DELL.Files");
            DropTable("DELL.Downloads");
        }
    }
}
