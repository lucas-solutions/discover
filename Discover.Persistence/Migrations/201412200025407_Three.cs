namespace Discover.Solutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Three : DbMigration
    {
        public override void Up()
        {
            AlterColumn("DELL.Files", "Archive", c => c.String(maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("DELL.Files", "Archive", c => c.String(nullable: false, maxLength: 255, unicode: false));
        }
    }
}
