namespace Discover.Solutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Four : DbMigration
    {
        public override void Up()
        {
            AlterColumn("DELL.Files", "Path", c => c.String(maxLength: 200, unicode: false));
            AlterColumn("DELL.Files", "Query", c => c.String(maxLength: 200, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("DELL.Files", "Query", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("DELL.Files", "Path", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
