namespace Discover.Solutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Four1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("DELL.Links", "Text", c => c.String(nullable: false, maxLength: 1000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("DELL.Links", "Text", c => c.String(nullable: false, maxLength: 255, unicode: false));
        }
    }
}
