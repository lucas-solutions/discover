﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class WebSpiderEntrySorter : IComparer<WebSpiderEntry>
    {
        public int Compare(WebSpiderEntry x, WebSpiderEntry y)
        {
            var cr = 0;

            if (x.Download && !y.Download) cr -= short.MaxValue;

            if (!x.Download && y.Download) cr += short.MaxValue;

            if (!x.DownloadUtc.HasValue && y.DownloadUtc.HasValue) cr -= 10;

            if (x.DownloadUtc.HasValue && !y.DownloadUtc.HasValue) cr += 10;

            if (x.Priority > y.Priority) return cr -= (x.Priority - y.Priority);

            if (x.Priority < y.Priority) return cr += (y.Priority - x.Priority);

            if (x.DownloadUtc.HasValue && y.DownloadUtc.HasValue && x.DownloadUtc.Value < y.DownloadUtc.Value) cr -= (int)(y.DownloadUtc.Value - x.DownloadUtc.Value).TotalMinutes / 6;

            if (x.DownloadUtc.HasValue && y.DownloadUtc.HasValue && x.DownloadUtc.Value > y.DownloadUtc.Value) cr += (int)(x.DownloadUtc.Value - y.DownloadUtc.Value).TotalMinutes / 6;

            return cr;
        }
    }
}
