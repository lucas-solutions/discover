﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    [Serializable]
    public class HtmlDownloadResult : ResourceDownloadResult
    {
        const string HtmlContentType = "text/html";

        internal static HtmlDownloadResult Ok(WebSpiderEntry entry, HtmlAgilityPack.HtmlDocument document)
        {
            return new HtmlDownloadResult(entry, document, System.Diagnostics.Process.GetCurrentProcess().Id);
        }

        protected HtmlDownloadResult(WebSpiderEntry entry, HtmlAgilityPack.HtmlDocument document, int downloadProcessId)
            : base(entry, null, downloadProcessId)
        {
            Document = document;
        }

        protected HtmlDownloadResult(WebSpiderEntry resource, int downloadProcessId, IEnumerable<string> errors)
            : base(resource, downloadProcessId, errors)
        {
        }

        internal HtmlAgilityPack.HtmlDocument Document { get; private set; }

        public override IResult Save(DirectoryInfo folder)
        {
            var errors = new List<string>();

            var fileInfo = new FileInfo(Path.Combine(folder.FullName, Entry.LocalPath));

            if (Document == null) errors.Add("DownloadResult.Document is null");

            if (errors.Count > 0) return Result.Fail(errors);

            try
            {
                if (fileInfo.Exists) fileInfo.Delete();

                if (!fileInfo.Directory.Exists) fileInfo.Directory.Create();

                Trace.WriteLine(string.Format(@"Writing binary content to file '{0}'.", fileInfo));

                Document.Save(fileInfo.FullName);
            }
            catch (Exception e)
            {
                errors.Add(e.ToString());
            }

            return errors.Count.Equals(0) ? Result.Ok() : Result.Fail(errors);
        }
    }
}
