﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;

namespace Discover.Solutions.Web.Spider
{

    public delegate void ProcessingUrlEventHandler(object sender, ProcessingUrlEventArgs e);

    public delegate void ProcessCompletedEventHandler(object sender, RunWorkerCompletedEventArgs e);

    public class WebSpiderBase : IWebSpider, IDisposable
    {
        const string DefaultUserAgent = @"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
        const int MaxDepth = 100;

        /// <summary>
        /// 
        /// </summary>
        static readonly string[] AllowedExtensions =
            new string[] 
			{ 
				@".jpg",
				@".jpeg",
				@".png",
				@".gif",
				@".html",
				@".css",
				@".pdf",
				@".txt",
			};

        private string _solutionPath;
        private WebSpiderCollection _collection;
        private WebSpiderEntryQueue _queue;
        private BackgroundWorker asyncBackgroundWorker = null;
       
        public WebSpiderBase(WebSpiderProject project)
        {
            Trace.WriteLine(string.Format(@"Constructing WebSpider for URI '{0}', destination folder path '{1}'.", project.BaseUrl, SolutionFolder));

            Project = project;
        }

        ~WebSpiderBase()
        {
            ////settings.Persist();
        }

        public void Dispose()
        {
            ////settings.Persist();
        }

        /// <summary>
        /// Called when processing an URL.
        /// </summary>
        public event ProcessingUrlEventHandler ProcessingUrl;

        /// <summary>
        /// Called when the asynchron processing is completed.
        /// </summary>
        public event ProcessCompletedEventHandler ProcessCompleted;

        public WebSpiderCollection Collection
        {
            get { return _collection ?? (_collection = new WebSpiderCollection()); }
            protected set { _collection = value; }
        }

        public WebSpiderProject Project { get; private set; }

        public System.IO.DirectoryInfo SolutionFolder
        {
            get { return _solutionPath != null ? new System.IO.DirectoryInfo(_solutionPath) : null; }
            set { _solutionPath = value != null ? value.FullName : null; }
        }

        public WebSpiderEntryQueue Queue
        {
            get { return _queue ?? (_queue = new WebSpiderEntryQueue()); }
            protected set { _queue = value; }
        }

        /// <summary>
        /// If a proxy is required, apply it to the request.
        /// </summary>
        /// <param name="webClient">The req.</param>
        /// <param name="options">The options.</param>
        protected virtual void ApplyProxy(WebClient webClient)
        {
            var options = Project;

            switch (options.ProxyUsage)
            {
                default:
                case DownloadProxyUsage.Default:
                    webClient.Proxy = WebRequest.DefaultWebProxy;
                    break;

                case DownloadProxyUsage.NoProxy:
                    webClient.Proxy = null;
                    break;

                case DownloadProxyUsage.UseProxy:
                    webClient.Proxy = null;
                    //Debug.Assert(Project.Proxy != null);
                    //webClient.Proxy = Project.Proxy;
                    break;
            }
        }

        /// <summary>
        /// Performs the complete downloading (synchronously). 
        /// Does return only when completely finished or when an exception
        /// occured.
        /// </summary>
        public virtual IResult Download(ref bool stop)
        {
            var errors = new List<string>();

            /*var opLoad = Load();

            if (!opLoad.Succeeded) return Result.Fail(opLoad.Errors);*/

            string baseUrl = Project.BaseUrl.TrimEnd('/').Split('?')[0];

            // The URI that is configured to be the start URI.
            Uri baseUri = new Uri(baseUrl, UriKind.Absolute);

            // The initial seed.
            WebSpiderEntry root = new WebSpiderEntry
            {
                Download = true,
                Level = 0,
                RemoteUrl = @"/",
                ResourceType = UriType.Content
            };

            root.MakeAbsoulteRemoteUrl(baseUri);

            if (Collection.Count.Equals(0)) Collection.Add(root);

            Queue = new WebSpiderEntryQueue(MakeSortedList(Collection.Where(item => item.Download == true && item.DownloadUtc == null)));

            var queueFreshCount = Queue.Count;
            var collectionSafeCount = Collection.Count;

            // 2007-07-27, Uwe Keim:
            // Doing a multiple looping, to avoid stack overflows.
            // Since a download-"tree" (i.e. the hierachy of all downloadable
            // pages) can get _very_ deep, process one part at a time only.
            // The state is already persisted, so we need to set up again at
            // the previous position.
            int index = 0;
            while (Queue.Count > 0 && !stop)
            {
                // Fetch one.
                WebSpiderEntry entry = Queue.Dequeue();

                Trace.WriteLine(string.Format(@"{0}. loop: Starting processing URLs from '{1}'.", index + 1, entry.RemoteUrl));

                // Process the URI, add any continue URIs to start
                // again, later.
                // Download(entry);

                Trace.WriteLine(string.Format(@"Processing URI '{0}', with depth {1}.", entry.RemoteUrl, entry.Level));

                if (Project.MaximumEntryLevel > 0 && entry.Level > Project.MaximumEntryLevel)
                {
                    var error = string.Format(@"Depth {1} exceeds maximum configured depth. Ending recursion at URI '{0}'.", entry.RemoteUrl, entry.Level);

                    Trace.WriteLine(error);

                    errors.Add(error);

                    continue;
                }

                if (entry.Level > MaxDepth)
                {
                    var error = string.Format("Depth {1} exceeds maximum allowed recursion depth. Ending recursion at URI '{0}' to possible continue later.",
                        entry.RemoteUrl, entry.Level);

                    Trace.WriteLine(error);

                    errors.Add(error);

                    continue;
                };

                // If we are in asynchron mode, periodically check for stopps.
                if (asyncBackgroundWorker != null && asyncBackgroundWorker.CancellationPending) throw new StopProcessingException();

                // Notify event sinks about this URL.
                if (ProcessingUrl != null) ProcessingUrl(this, new ProcessingUrlEventArgs(entry, entry.Level));

                if (entry.External && Project.StayOnSite)
                {
                    Trace.WriteLine(string.Format(@"URI '{0}' is not processable. Skipping.", entry.RemoteUrl));

                    continue;
                }

                Trace.WriteLine(string.Format(@"Processing resource URI '{0}', with depth {1}.", entry.RemoteUrl, entry.Level));

                var downloadResult = Download(entry);

                if (!downloadResult.Succeeded)
                {
                    errors.AddRange(downloadResult.Errors);
                    continue;
                }

                downloadResult.Save(SolutionFolder);

                // Add before parsing childs.
                //_settings.AddDownloadedResourceInfo(resource);

                // Persist after completely parsed childs.
                //_settings.PersistDownloadedResourceInfo(resource);

                Trace.WriteLine(string.Format(@"Finished processing URI '{0}'.", entry.RemoteUrl));

                if (Queue.Count > (queueFreshCount + Project.RefreshQueueInterval))
                {
                    var queue = new WebSpiderEntryQueue(MakeSortedList(Queue.ToList()));

                    Queue.Clear();

                    GC.Collect();

                    Queue = queue;

                    queueFreshCount = Queue.Count;
                }

                if (Collection.Count > (collectionSafeCount + Project.SaveCollectionInterval))
                {
                    Save();

                    collectionSafeCount = Collection.Count;
                }

                index++;
            }

            Trace.WriteLine(string.Format(@"{0}. loop: Finished processing URLs from seed '{1}'.", index + 1, baseUrl));

            return errors.Count.Equals(0) ? Result.Ok() : Result.Fail(errors);
        }

        protected virtual IList<WebSpiderEntry> MakeSortedList(IEnumerable<WebSpiderEntry> source)
        {
            var list = source.ToList();

            list.Sort(new WebSpiderEntrySorter());

            return list;
        }

        /// <summary>
        /// Performs the complete downloading (asynchronously). 
        /// Return immediately. Calls the ProcessCompleted event
        /// upon completion.
        /// </summary>
        public void DownloadAsync()
        {
            asyncBackgroundWorker = new BackgroundWorker();

            asyncBackgroundWorker.WorkerSupportsCancellation = true;

            asyncBackgroundWorker.DoWork += OnAsyncDownloadDoWork;
            asyncBackgroundWorker.RunWorkerCompleted += OnAsyncDownloadRunWorkerCompleted;

            // Start.
            asyncBackgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancels a currently running asynchron processing.
        /// </summary>
        public void CancelDownloadAsync()
        {
            if (asyncBackgroundWorker != null)
            {
                asyncBackgroundWorker.CancelAsync();
            }
        }

        /// <summary>
        /// Donwload an entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns>Download result.</returns>
        protected virtual ResourceDownloadResult Download(WebSpiderEntry entry)
        {
            ResourceDownloadResult result = null;

            entry.DownloadStatus = HttpStatusCode.Redirect;

            var absoluteUri = new Uri(entry.RemoteUrl, UriKind.Absolute);

            Debug.WriteLine(@"Reading content from URL '{0}'.", absoluteUri);

            try
            {
                using (var webClient = new WebClient())
                {
                    ApplyProxy(webClient);

                    webClient.Headers.Add("user-agent", DefaultUserAgent);

                    webClient.CachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache);

                    var stream = webClient.OpenRead(absoluteUri);

                    using (var mem = new MemoryStream())
                    {
                        stream.CopyTo(mem);

                        entry.DownloadStatus = HttpStatusCode.OK;
                        entry.DownloadUtc = DateTime.UtcNow;
                        entry.ContentType = webClient.ResponseHeaders["Content-Type"];
                        entry.ContentDisposition = webClient.ResponseHeaders["Content-Disposition"];

                        entry.MakeLocalPath();

                        if (entry.Page)
                        {
                            try
                            {
                                var document = new HtmlDocument();

                                mem.Seek(0, SeekOrigin.Begin);

                                document.Load(mem);

                                var errors = document.ParseErrors.ToList();

                                var ok = Process(entry, document);

                                result = HtmlDownloadResult.Ok(entry, document);
                            }
                            catch (Exception e)
                            {
                                entry.Page = false;
                            }
                        }

                        if (entry.Page == false)
                        {
                            mem.Seek(0, SeekOrigin.Begin);

                            var content = mem.GetBuffer();

                            result = ResourceDownloadResult.Ok(entry, content);
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    HttpWebResponse resp = (HttpWebResponse)e.Response;

                    entry.DownloadStatus = resp.StatusCode;
                    entry.DownloadUtc = null;

                    entry.Errors.Add(new WebSpiderError { ErrorUtc = DateTime.UtcNow, Project = entry.Project, Entry = entry, ErrorCode = (int)entry.DownloadStatus, Error = e.ToString() });

                    result = ResourceDownloadResult.Fail(entry, new List<string> { e.ToString() });

                    Trace.WriteLine(string.Format(@"Http Protocol Error: {0}, '{1}'.", entry.DownloadStatus, e.ToString()));
                }
                else
                {
                    entry.Errors.Add(new WebSpiderError { ErrorUtc = DateTime.UtcNow, Project = entry.Project, Entry = entry, ErrorCode = (int)entry.DownloadStatus, Error = e.ToString() });

                    result = ResourceDownloadResult.Fail(entry, new List<string> { e.ToString() });

                    Trace.WriteLine(string.Format(@"Web Request Error: '{0}'.", e.ToString()));
                }
            }
            catch (Exception e)
            {
                result = ResourceDownloadResult.Fail(entry, new List<string> { e.ToString() });

                entry.Errors.Add(new WebSpiderError { ErrorUtc = DateTime.UtcNow, Project = entry.Project, Entry = entry, ErrorCode = (int)entry.DownloadStatus, Error = e.ToString() });

                Trace.WriteLine(string.Format(@"General Error: '{0}'.", e.ToString()));
            }

            return result;
        }

        public virtual IResult Load()
        {
            if (Project == null) return Result.Fail("Project undefined");

            IResult result = Result.Ok();

            using (var store = IoC.GetInstance<IWebSpiderProjectEntityStore>())
            {
                WebSpiderProject entry = null;

                if (Project.Id != 0) entry = store.FindById(Project);

                if (entry == null) store.FindByUrl(Project.BaseUrl);

                if (entry != null)
                {
                    var list = entry.Entries.ToList();

                    foreach (var item in list)
                    {
                        Queue.Enqueue(item);
                    }
                }
                else
                {
                    var opCreate = store.Create(Project);

                    if (!opCreate.Succeeded) return Result.Fail(opCreate.Errors);

                    entry = opCreate.Value;

                    var root = new WebSpiderEntry
                    {
                        RemoteUrl = Project.BaseUrl,
                        Download = true
                    };

                    entry.Entries.Add(root);

                    Queue.Enqueue(root);

                    store.SaveChanges();
                }
            }

            return result;
        }

        protected virtual IResult Process(WebSpiderEntry entry, HtmlDocument document)
        {
            var links = document.DocumentNode.ExtractLinks(entry);

            foreach (WebSpiderEntry link in links)
            {
                var uri = new Uri(link.RemoteUrl, UriKind.Absolute);

                link.Download = (!link.External || !Project.StayOnSite);

                Rate(link);

                if (Collection.Contains(uri))
                {
                    var item = Collection[uri];

                    if (link.Level < item.Level) item.Level = link.Level;
                }
                else
                {
                    Collection.Add(link);

                    if (link.Download) Queue.Enqueue(link);
                }
            }

            document.DocumentNode.RemoveScripts();

            return Result.Ok(true);
        }

        protected virtual byte Rate(WebSpiderEntry entry)
        {
            if (entry.Page) entry.Priority = (byte)Math.Max(255, entry.Priority + 100);

            return entry.Priority;
        }

        void OnAsyncDownloadDoWork(object sender, DoWorkEventArgs e)
        {
            bool stop = false;

            try
            {
                Download(ref stop);
            }
            catch (StopProcessingException)
            {
                // Do nothing, just end.
            }
        }

        protected virtual void OnAsyncDownloadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (ProcessCompleted != null)
            {
                ProcessCompleted(this, e);
            }
        }

        public static WebSpiderBase Open(string solutionPath, WebSpiderProject project)
        {
            var path = Path.Combine(solutionPath, new Uri(project.BaseUrl, UriKind.Absolute).Host);

            var opRestore = WebSpiderStore.Restore(new DirectoryInfo(path));

            var collection = new WebSpiderCollection();

            foreach (var entry in opRestore.Value.Entries)
            {
                collection.Add(entry);
            }

            project.Entries = collection;

            return new WebSpiderBase(project)
            {
                _collection = collection,
                _solutionPath = solutionPath
            };
        }

        public IResult Save()
        {
            var path = Path.Combine(_solutionPath, new Uri(Project.BaseUrl, UriKind.Absolute).Host);

            Project.Entries = Collection.ToList();

            var result = Project.Persist(new DirectoryInfo(path));

            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class StopProcessingException : Exception
    {
    }
}
