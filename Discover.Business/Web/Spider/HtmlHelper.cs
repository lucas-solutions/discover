﻿using Discover.Solutions.Json.Linq;
using Discover.Solutions.Web.Spider;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    internal static class HtmlHelper
    {
        const string UserAgent = @"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
 
        /// <summary>
        /// Get all links from the text content.
        /// </summary>
        /// <returns></returns>
        public static List<WebSpiderEntry> ExtractLinks(this HtmlAgilityPack.HtmlNode root, WebSpiderEntry baseEntry)
        {
            var result = new List<WebSpiderEntry>();

            var baseUri = new Uri(baseEntry.RemoteUrl, UriKind.Absolute);
            byte level = (byte)Math.Min(255, baseEntry.Level + 1);

            foreach (var linkElement in LinkElement.LinkElements)
            {
                var selection = root.SelectNodes(@"//" + linkElement.Name);

                if (selection == null) continue;

                foreach (var node in selection)
                {
                    foreach (var attr in node.Attributes)
                    {
                        result.AddRange(ExtractStyleUrls(attr, level, baseUri));

                        foreach (string linkAttr in linkElement.Attributes)
                        {
                            if (string.Compare(linkAttr, attr.Name, true) == 0)
                            {
                                var originalUrl = attr.Value;

                                if (originalUrl.StartsWith(@"#")) continue;

                                var link = new WebSpiderEntry
                                    {
                                        Level = level,
                                        RemoteUrl = originalUrl,
                                        ResourceType = linkElement.LinkType
                                    };

                                link.MakeAbsoulteRemoteUrl(baseUri);

                                result.Add(link);
                            }
                        }
                    }
                }
            }

            // Trace the extracted links.
            int index = 0;
            foreach (var entry in result)
            {
                Trace.WriteLine(string.Format(@"Extracted link {0}/{1}: Found '{2}' in document at URI '{3}'.",
                        index + 1,
                        result.Count,
                        entry.RemoteUrl,
                        baseEntry.RemoteUrl));

                index++;
            }

            return result;
        }

        /// <summary>
        /// Detects URLs in styles.
        /// </summary>
        /// <param name="baseUri">The base URI.</param>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <param name="attributeValue">The attribute value.</param>
        /// <returns></returns>
        public static List<WebSpiderEntry> ExtractStyleUrls(this HtmlAgilityPack.HtmlAttribute htmlAttribute, byte level, Uri baseUri)
        {
            var result = new List<WebSpiderEntry>();

            if (string.Compare(htmlAttribute.Name, @"style", true) == 0)
            {
                if (htmlAttribute.Value != null && htmlAttribute.Value.Trim().Length > 0)
                {
                    var matchs = Regex.Matches(htmlAttribute.Value, @"url\s*\(\s*([^\)\s]+)\s*\)", RegexOptions.Singleline | RegexOptions.IgnoreCase);

                    if (matchs.Count > 0)
                    {
                        foreach (Match match in matchs)
                        {
                            if (match != null && match.Success)
                            {
                                string url = match.Groups[1].Value;

                                var link = new WebSpiderEntry
                                {
                                    Level = level,
                                    RemoteUrl = url,
                                    ResourceType = UriType.Resource
                                };

                                link.MakeAbsoulteRemoteUrl(baseUri);

                                result.Add(link);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static IResult<int> RemoveScripts(this HtmlAgilityPack.HtmlNode root)
        {
            var selection = root.SelectNodes(@"//script");

            if (selection == null) return Result.Ok(0);

            var count = 0;

            foreach (var node in selection)
            {
                node.Remove();
                count++;
            }

            return Result.Ok(count);
        }
    }
}
