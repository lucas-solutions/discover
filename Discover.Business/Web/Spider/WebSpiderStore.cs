﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public static class WebSpiderStore
    {
        const string FileName = @"WebSpider.project";

        const string BackupName = @"WebSpider.backup";

        /// <summary>
        /// Restore a previously stored setting value from the given
        /// folder path.
        /// </summary>
        /// <returns>Returns an empty object if not found.</returns>
        public static IResult<WebSpiderProject> Restore(DirectoryInfo folderPath)
        {
            string fileName = Path.Combine(folderPath.FullName, FileName);

            if (!File.Exists(fileName)) return Result<WebSpiderProject>.Fail("File \"{0}\" not found.", fileName);

            IResult<WebSpiderProject> result;

            try
            {
                var serializer = new JsonSerializer();

                var project = serializer.Deserialize<WebSpiderProject>(new JsonTextReader(new StreamReader(fileName)));

                result = Result<WebSpiderProject>.Ok(project);
            }
            catch (SerializationException x)
            {
                result = Result<WebSpiderProject>.Fail(string.Format(@"Ignoring exception while deserializing spider project: '{0}'.", x.ToString()));

                Trace.WriteLine(result.Errors.Last());
            }
            catch (IOException x)
            {
                result = Result<WebSpiderProject>.Fail(string.Format(@"Ignoring IO exception while loading spider project: '{0}'.", x.ToString()));

                Trace.WriteLine(result.Errors.Last());
            }
            catch (UnauthorizedAccessException x)
            {
                result = Result<WebSpiderProject>.Fail(string.Format(@"Ignoring exception while loading spider settings: '{0}'.", x.ToString()));

                Trace.WriteLine(result.Errors.Last());
            }
            catch (Exception e)
            {
                result = Result<WebSpiderProject>.Fail(string.Format(@"Ignoring exception while deserializing spider project: '{0}'.", e.ToString()));

                Trace.WriteLine(result.Errors.Last());
            }

            return result;
        }

        /// <summary>
        /// Persistently stores this object.
        /// </summary>
        public static IResult Persist(this WebSpiderProject project, DirectoryInfo folderPath)
        {
            IResult result;

            string fileName = Path.Combine(folderPath.FullName, FileName);

            try
            {
                if (File.Exists(fileName))
                {
                    var backupName = Path.Combine(folderPath.FullName, BackupName);

                    if (File.Exists(backupName)) File.Delete(backupName);

                    File.Move(fileName, backupName);
                }

                var contractResolver = new WebSpiderContractResolver();

                contractResolver.Exclude<WebSpiderEntry>(entry => entry.Id);
                contractResolver.Exclude<WebSpiderEntry>(entry => entry.Project);
                contractResolver.Exclude<WebSpiderEntry>(entry => entry.ProjectId);
                contractResolver.Exclude<WebSpiderEntry>(entry => entry.Errors);
                contractResolver.Exclude<WebSpiderProject>(proj => proj.Id);
                contractResolver.Exclude<WebSpiderProject>(proj => proj.Errors);

                var serializer = new JsonSerializer
                {
                    Formatting = Formatting.Indented,
                    ContractResolver = contractResolver
                };

                using (var writer = new StreamWriter(fileName))
                {
                    serializer.Serialize(new JsonTextWriter(writer), project, typeof(WebSpiderProject));

                    writer.Flush();
                    writer.Close();
                }

                result = Result.Ok();
            }
            catch (IOException x)
            {
                result = Result.Fail(string.Format(@"Ignoring IO exception while persisting spider settings: '{0}'.", x.Message));

                Trace.WriteLine(result.Errors.Last());
            }
            catch (UnauthorizedAccessException x)
            {
                result = Result.Fail(string.Format(@"Ignoring exception while persisting spider settings: '{0}'.", x.Message));

                Trace.WriteLine(result.Errors.Last());
            }
            catch (Exception e)
            {
                result = Result.Fail(e.ToString());

                Trace.WriteLine(result.Errors.Last());
            }

            return result;
        }
    }
}
