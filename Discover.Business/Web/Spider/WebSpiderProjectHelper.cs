﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public static class WebSpiderProjectHelper
    {
        /// <summary>
        /// Returns non-NULL if a proxy is required.
        /// </summary>
        /// <value>The proxy.</value>
        public static IWebProxy Proxy(this WebSpiderProject project)
        {
            WebProxy proxy = null;

            if (project.ProxyUsage == DownloadProxyUsage.UseProxy)
            {
                proxy = new WebProxy();

                proxy.Address = new Uri(ConfigurationManager.AppSettings[@"downloadProxyAddress"]);
                proxy.BypassProxyOnLocal = ConvertToBoolean(ConfigurationManager.AppSettings[@"downloadProxyBypassProxyOnLocal"],
                    true);

                proxy.UseDefaultCredentials =
                    ConvertToBoolean(
                    ConfigurationManager.AppSettings[@"downloadProxyUseDefaultCredentials"],
                    true);

                if (!proxy.UseDefaultCredentials)
                {
                    NetworkCredential credentials = new NetworkCredential();

                    credentials.Domain = ConvertToString(ConfigurationManager.AppSettings[@"downloadProxyCredentialsDomain"], string.Empty);

                    credentials.Password = ConvertToString(ConfigurationManager.AppSettings[@"downloadProxyCredentialsPassword"], string.Empty);

                    credentials.UserName = ConvertToString(ConfigurationManager.AppSettings[@"downloadProxyCredentialsUserName"], string.Empty);

                    proxy.Credentials = credentials;
                }
            }

            return proxy;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="fallbackTo">The fallback to.</param>
        /// <returns></returns>
        private static string ConvertToString(object o, string fallbackTo)
        {
            if (o == null)
            {
                return fallbackTo;
            }
            else
            {
                return o.ToString();
            }
        }

        /// <summary>
        /// Converts to boolean.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="fallbackTo">if set to <c>true</c> [fallback to].</param>
        /// <returns></returns>
        private static bool ConvertToBoolean(
            object text,
            bool fallbackTo)
        {
            if (text != null && IsBoolean(text))
            {
                return Convert.ToBoolean(text);
            }
            else
            {
                return fallbackTo;
            }
        }

        /// <summary>
        /// Checks whether a string contains a valid boolean.
        /// </summary>
        /// <param name="o">The string to check.</param>
        /// <returns>
        /// Returns TRUE if is a boolean, FALSE if not.
        /// </returns>
        public static bool IsBoolean(
            object o)
        {
            try
            {
                if (o == null || o.ToString().Trim().Length <= 0 ||
                    (
                    o.ToString().Trim().ToLower() != bool.TrueString.ToLower() &&
                    o.ToString().Trim().ToLower() != bool.FalseString.ToLower()
                    ))
                {
                    return false;
                }
                else
                {
                    bool.Parse(o.ToString());
                }
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }
    }
}
