﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public class ProcessingUrlEventArgs : EventArgs
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        internal ProcessingUrlEventArgs(WebSpiderEntry uriInfo, int depth)
        {
            Entry = uriInfo;
            Depth = depth;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Depth { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public WebSpiderEntry Entry { get; set; }
    }
}
