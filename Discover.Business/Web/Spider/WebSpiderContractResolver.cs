﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    internal class WebSpiderContractResolver : DefaultContractResolver
    {
        private readonly List<string> _exlude = new List<string>();

        public void Exclude<T>(Expression<Func<T, object>> action)
            where T : class
        {
            var expression = GetMemberInfo(action);
            string typeName = typeof(T).Name;
            string propName = expression.Member.Name;
            _exlude.Add(typeName + '@' + propName);
        }

        public void Exclude<T>(params string[] propertyNameToExclude)
            where T : class
        {
            Exclude(typeof(T).Name, propertyNameToExclude);
        }

        public void Exclude(string typeName, params string[] propertyNameToExclude)
        {
            foreach (var propName in propertyNameToExclude)
            {
                _exlude.Add(typeName + '@' + propName);
            }
        }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);

            // only serializer properties that are not named after the specified property.
            properties = properties.Where(p => !_exlude.Contains(p.DeclaringType.Name + "@" + p.PropertyName)).ToList();

            return properties;
        }

        private static MemberExpression GetMemberInfo(Expression method)
        {
            LambdaExpression lambda = method as LambdaExpression;
            if (lambda == null)
                throw new ArgumentNullException("method");

            MemberExpression memberExpr = null;

            if (lambda.Body.NodeType == ExpressionType.Convert)
            {
                memberExpr =
                    ((UnaryExpression)lambda.Body).Operand as MemberExpression;
            }
            else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpr = lambda.Body as MemberExpression;
            }

            if (memberExpr == null)
                throw new ArgumentException("method");

            return memberExpr;
        }
    }
}
