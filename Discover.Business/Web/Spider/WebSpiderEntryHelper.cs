﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    public static class WebSpiderEntryHelper
    {
        static string[] HtmlMimeTypes = new[]
            {
                ".html"
            };

        static Dictionary<string, string> MimeTypes = new Dictionary<string, string>
        {
            { "application/ogg", ".ogg" },
            { "application/pdf", ".pdf" },
            { "application/postscript", ".ps" },
            { "application/rdf+xml", ".xml" },
            { "application/rss+xml", ".xml" },
            { "application/soap+xml", "xml" },
            { "application/font-woff", ".woff" },
            { "application/xhtml+xml", ".html" },
            { "application/xml", ".xml" },
            { "application/xml-dtd", ".xml" },
            { "application/xop+xml", ".xml" },
            { "application/zip", ".zip" },
            { "application/gzip", ".gzip" },
            { "application/example", ".example" },
            { "audio/basic", ".audio" },
            { "audio/L24", ".l24" },
            { "audio/mp4", ".mp4" },
            { "audio/mpeg", ".mpeg" },
            { "audio/ogg", ".ogg" },
            { "audio/opus", ".opus" },
            { "audio/vorbis", ".vorbis" },
            { "audio/vnd.rn-realaudio", ".realaudio" },
            { "audio/vnd.wave", ".wav" },
            { "audio/webm", "webm" },
            { "audio/example", ".example" },
            { "image/gif", ".gif" },
            { "image/jpeg", ".jpg" },
            { "image/png", ".png" },
            { "image/svg+xml", ".svg" },
            { "image/tiff", ".tiff" },
            { "image/vnd.djvu", ".djvu" },
            { "image/example", ".example" },
            { "text/css", ".css" },
            { "text/csv", ".cvs" },
            { "text/html", ".html" },
            { "text/javascript", ".js" },
            { "text/plain", ".txt" },
            { "text/rtf", ".rtf" },
            { "text/vcard", ".vcard" },
            { "text/vnd.abc", ".abc" },
            { "text/xml", ".xml" }
        };

        public static string MakeLocalPath(this WebSpiderEntry entry)
        {
            var absoluteUri = new Uri(entry.RemoteUrl, UriKind.Absolute);

            var paths = new List<string>() { absoluteUri.Host };

            paths.AddRange(absoluteUri.AbsolutePath.Split(new[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries));

            string fileExtension = null;

            if (!string.IsNullOrEmpty(entry.ContentType))
            {
                fileExtension = entry.ContentType.Split(new[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(mime =>
                    {
                        string value = null;
                        MimeTypes.TryGetValue(mime, out value);
                        return value;
                    })
                    .FirstOrDefault(value => value != null)
                    
                    ?? entry.ContentType.Split(new[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(mime => mime.Contains('/'))
                    .Select(mime => mime.Split('/')[1])
                    .FirstOrDefault();

                if (HtmlMimeTypes.Contains(fileExtension)) entry.Page = true;
            }

            var fileName = DefaultFileName(entry) ?? ContentFileName(entry) ?? QueryFileName(entry);

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = paths.Last() + fileExtension;

                paths.RemoveAt(paths.Count - 1);
            }

            paths.Add(fileName);

            entry.LocalPath = Path.Combine(paths.ToArray());

            return entry.LocalPath;
        }

        private static string ContentFileName(WebSpiderEntry entry)
        {
            string fileName = null;

            // Try to extract the filename from the Content-Disposition header
            if (!string.IsNullOrEmpty(entry.ContentDisposition))
            {
                var contentDisposition = new ContentDisposition(entry.ContentDisposition);

                 fileName = contentDisposition.FileName;
            }

            return !string.IsNullOrEmpty(fileName) ? fileName : null;
        }

        private static string DefaultFileName(WebSpiderEntry entry)
        {
            if (entry.Page == true && new Uri(entry.RemoteUrl, UriKind.Absolute).PathAndQuery == "/") return "index.html";

            return null;
        }

        private static string QueryFileName(WebSpiderEntry entry)
        {
            string fileName = null;

            if (entry.Page)
            {
                fileName = new Uri(entry.RemoteUrl, UriKind.Absolute).Query;

                if (!string.IsNullOrEmpty(fileName))
                {
                    var parts = fileName.TrimStart('?').Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries).Select(param => Normalize(param)).OrderBy(value => value).ToList();

                    var hash = new List<string>();

                    fileName = string.Join("_", parts);

                    while (fileName.Length > 50)
                    {
                        hash.Add(parts[parts.Count - 1]);
                        parts.RemoveAt(parts.Count - 1);

                        fileName = string.Join("_", parts) + "_hash=" + string.Join("_", hash).GetHashCode().ToString("X8");
                    }

                    fileName += ".html";
                }
            }

            return fileName;
        }

        private static string Normalize(string value)
        {
            var parts = value.Split(new [] { ':', '/', '~', '*', '+', '-', ';', '?', '\\' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            parts.RemoveAll(s => s == "amp");
            parts.RemoveAll(s => s == "com");
            parts.RemoveAll(s => s == "http");
            parts.RemoveAll(s => s == "https");

            return string.Join("-", parts);
        }

        /// <summary>
        /// Makes the absolute URI.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="baseUri">The base URI.</param>
        public static Uri MakeAbsoulteRemoteUrl(this WebSpiderEntry entry, Uri baseUri)
        {
            Uri uri;

            if (Uri.IsWellFormedUriString(entry.RemoteUrl, UriKind.Absolute))
            {
                uri = new Uri(entry.RemoteUrl, UriKind.Absolute);

                entry.RemoteUrl = uri.AbsoluteUri;

                entry.External = string.Compare(baseUri.Host, uri.Host, true) != 0;

                return uri;
            }

            entry.External = false;

            string[] uriParts = entry.RemoteUrl.Split('?');

            UriBuilder uriBuilder = new UriBuilder(baseUri.AbsoluteUri);

            /*if (entry.RemoteUrl.StartsWith("/"))
            {*/
                uriBuilder.Path = uriParts[0];
            /*}
            else
            {
                uriBuilder.Path = CombineVirtualPath(uriBuilder.Path, uriParts[0]);
            }*/

            if (uriParts.Length > 1) uriBuilder.Query = uriParts[1];

            uri = uriBuilder.Uri;

            entry.RemoteUrl = uri.AbsoluteUri;

            return uri;
        }

        /// <summary>
        /// Combines the virtual path.
        /// </summary>
        /// <param name="s1">The s1.</param>
        /// <param name="s2">The s2.</param>
        /// <returns></returns>
        private static string CombineVirtualPath(string s1, string s2)
        {
            if (string.IsNullOrEmpty(s1)) return s2;

            if (string.IsNullOrEmpty(s2)) return s1;

            s1 = s1.TrimEnd('/');
            s2 = s2.TrimStart('/');

            return s1 + @"/" + s2;
        }
    }
}
