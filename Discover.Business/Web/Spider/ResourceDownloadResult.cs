﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Web.Spider
{
    [Serializable]
    public class ResourceDownloadResult : IResult<byte[]>
    {
        internal static ResourceDownloadResult Fail(WebSpiderEntry entry, IEnumerable<string> errors)
        {
            return new ResourceDownloadResult(entry, System.Diagnostics.Process.GetCurrentProcess().Id, errors);
        }

        internal static ResourceDownloadResult Ok(WebSpiderEntry entry, byte[] content)
        {
            return new ResourceDownloadResult(entry, content, System.Diagnostics.Process.GetCurrentProcess().Id);
        }

        protected ResourceDownloadResult(WebSpiderEntry resource, byte[] content, int downloadProcessId)
        {
            Content = content;
            DownloadProcessId = downloadProcessId;
            Entry = resource;
        }

        protected ResourceDownloadResult(WebSpiderEntry resource, int downloadProcessId, IEnumerable<string> errors)
        {
            DownloadProcessId = downloadProcessId;
            Errors = new List<string>(errors);
            Entry = resource;
        }

        public byte[] Content { get; private set; }

        /// <summary>
        /// Gets the added by process ID.
        /// </summary>
        /// <value>The added by process ID.</value>
        public int DownloadProcessId { get; private set; }

        public ICollection<string> Errors { get; private set; }

        public WebSpiderEntry Entry { get; private set; }

        public bool Succeeded { get { return Errors == null; } }

        byte[] IResult<byte[]>.Value { get { return Content; } }

        object IResult.Value { get { return Content; } }

        public virtual IResult Save(DirectoryInfo folder)
        {
            var errors = new List<string>();

            var fileInfo = new FileInfo(Path.Combine(folder.FullName, Entry.LocalPath));

            if (Content == null) errors.Add("DownloadResult.Content is null");

            if (errors.Count > 0) return Result.Fail(errors);

            try
            {
                if (fileInfo.Exists) fileInfo.Delete();

                if (!fileInfo.Directory.Exists) fileInfo.Directory.Create();

                Trace.WriteLine(string.Format(@"Writing binary content to file '{0}'.", fileInfo));

                using (FileStream stream = fileInfo.OpenWrite())
                {
                    stream.Write(Content, 0, Content.Length);
                }
            }
            catch (Exception e)
            {
                errors.Add(e.ToString());
            }

            return errors.Count.Equals(0) ? Result.Ok() : Result.Fail(errors);
        }
    }
}
