﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Discover.Solutions.Json.Linq
{
    public class JQuery
    {
        private ICollection<JQuery> _fields;

        public JQuery()
            : this(null, null, null)
        {
        }

        public JQuery(string xpath)
            : this(null, xpath, new JQuery[0])
        {
        }

        public JQuery(string propName, string xpath)
            : this(propName, xpath, new JQuery[0])
        {
        }

        public JQuery(string propName, string xpath, params JQuery[] fields)
        {
            Name = propName;
            XPath = xpath;
            _fields = fields;
        }

        /// <summary>
        /// Property name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Html path
        /// </summary>
        public string XPath { get; set; }

        public ICollection<JQuery> Fields
        {
            get { return _fields ?? (_fields = new Collection<JQuery>()); }
            set { _fields = value; }
        }

        internal IResult<JArray> Match(HtmlNode node)
        {
            JArray result = new JArray();

            if (string.IsNullOrEmpty(XPath))
            {
                return Result<JArray>.Ok(result);
            }

            HtmlAgilityPack.HtmlNodeCollection children;

            if (XPath.StartsWith("@"))
            {
                var value = new JValue(node.GetAttributeValue(XPath.Substring(1), null));

                result.Add(value);

                return Result<JArray>.Ok(result);
            }

            children = node.SelectNodes(XPath);

            if (children == null || children.Count == 0)
            {
                return Result<JArray>.Fail(string.Format("{0} Not found in {1}", XPath, node.ToString()));
            }

            if (Fields.Count.Equals(0))
            {
                result = new JArray(children.Select(child => child.InnerText).ToArray());

                return Result<JArray>.Ok(result);
            }

            var errors = new List<string>();

            foreach (var child in children.ToArray())
            {
                var item = new JObject();

                result.Add(item);

                foreach (JQuery field in Fields)
                {
                    var res = field.Match(child);
                    item[field.Name] = res.Value;
                    errors.AddRange(res.Errors);
                }
            }

            return errors.Count.Equals(0) ? Result<JArray>.Ok(result) : Result<JArray>.Fail(errors);
        }
    }

    public abstract class JQuery<TValue> : JQuery
    {
        protected JQuery(string propName, string xpath, params JQuery[] fields)
            : base(propName, xpath, fields)
        {
        }

        internal IResult<TValue> Value(HtmlNode node)
        {
            IResult<TValue> result;

            var match = Match(node);

            if (!match.Succeeded) return Result<TValue>.Fail(match.Errors);

            try
            {
                switch (match.Value.Type)
                {
                    case JTokenType.Object:
                        result = Result<TValue>.Ok((match as JObject).ToObject<TValue>());
                        break;

                    default:
                        result = Result<TValue>.Ok(match.Value.Value<TValue>());
                        break;
                }
            }
            catch (Exception e)
            {
                result = Result<TValue>.Fail(e.ToString());
            }

            return result;
        }

        internal IEnumerable<TValue> Select(HtmlNode node)
        {
            var match = Match(node);

            if (!match.Succeeded && match.Value == null) return new TValue[0];

            var array = match.Value as JArray;

            if (array == null) return new TValue[0];

            return array.Where(obj => obj != null && obj.Type == JTokenType.Object).Select(obj => obj.ToObject<TValue>());
        }
    }
}
