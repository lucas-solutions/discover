﻿using System;
using System.Collections.Generic;

namespace Discover.Solutions.Dell
{
    using Discover.Solutions.Dell.Html;
    using Discover.Solutions.Web.Spider;
    using HtmlAgilityPack;
    using System.IO;

    internal class DellHtmlScanner
    {
        static Dictionary<string, string> Replace = new Dictionary<string, string>
        {
            { "â€”", "-" },
            { "â€™", "'" },
            { "â„¢", "™" },
            { "Â", "" }
        };

        static string Fix(string s)
        {
            foreach (var entry in Replace) s = s.Replace(entry.Key, entry.Value);
            return s;
        }

        public IEnumerable<DellProduct> Scan(WebSpiderEntry entry, HtmlDocument document)
        {
            var meta = MetaInfo.Select(document.DocumentNode);

            if (meta.OpenGraph.Type != "product") yield break;

            var product = new DellProduct
            {
                CategoryPath = meta.CategoryPath,
                ContentUrl = entry.RemoteUrl,
                ImageUrl = meta.OpenGraph.Image,
                DateUtc = DateTime.UtcNow,
                Description = Fix(meta.OpenGraph.Description),
                Details = new[]
                    {
                        new DellProductDetail
                        {
                            ContentUrl = entry.RemoteUrl,
                            Country = meta.Country,
                            DateUtc = DateTime.UtcNow,
                            ImageUrl = meta.OpenGraph.Image,
                            Language = meta.Language
                        }
                    },
                Segment = meta.Segment,
                Title = Fix(meta.OpenGraph.Title),
                Url = meta.OpenGraph.Url,
            };

            yield return product;
        }

        public IEnumerable<DellProduct> Scan(WebSpiderEntry entry)
        {
            var file = new FileInfo(entry.LocalPath);

            if (!file.Exists) yield break;

            var document = new HtmlDocument();

            using (var stream = file.OpenRead())
            {
                document.Load(stream);
            }

            foreach (var product in Scan(entry, document))
            {
                yield return product;
            }
        }

        public IEnumerable<DellProduct> Scan(params WebSpiderEntry[] entries)
        {
            return Scan((IEnumerable<WebSpiderEntry>) entries);
        }

        public IEnumerable<DellProduct> Scan(IEnumerable<WebSpiderEntry> entries)
        {
            foreach (var entry in entries)
            {
                var file = new FileInfo(entry.LocalPath);

                if (!file.Exists) continue;

                var document = new HtmlDocument();

                using (var stream = file.OpenRead())
                {
                    document.Load(stream);
                }

                foreach (var product in Scan(entry, document))
                {
                    yield return product;
                }
            }
        }
    }
}
