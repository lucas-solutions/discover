﻿using Discover.Solutions.Json.Linq;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Discover.Solutions.Dell.Html
{
    internal class Product
    {
        public string[] StartingAt { get; set; }

        public class Query : JQuery<Product>
        {
            public Query(string propName = null)
                : base(propName, "//div[@class=\"shortPriceStack\"]",
                    new JQuery("StartingAt", @"//span[@class=""retailSmallPrice""]")) { }
        }
    }
}
