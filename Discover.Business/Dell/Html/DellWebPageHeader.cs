﻿using Discover.Solutions.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Templates
{
    internal class DellWebPageHeader : JQuery
    {
        public DellWebPageHeader()
        {
            XPath = "//html";
            Fields = new Collection<JQuery>
            {
                new JQuery {Name = "Class", XPath = "@class"},
                new JQuery {Name = "Dir", XPath = "@dir"},
                new JQuery {Name = "Lang", XPath = "@lang"},
                new JQuery {Name = "ItemScope", XPath = "@itemscope"},
                new JQuery {Name = "ItemType", XPath = "@itemtype"}
            };
        }

        public string Class
        {
            get { return Result.Value<string>("Class"); }
        }

        public string Dir
        {
            get { return Result.Value<string>("Dir"); }
        }

        public string Lang
        {
            get { return Result.Value<string>("Lang"); }
        }

        public string ItemScope
        {
            get { return Result.Value<string>("ItemScope"); }
        }

        public string ItemType
        {
            get { return Result.Value<string>("ItemType"); }
        }
    }
}
