﻿using Discover.Solutions.Json.Linq;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Discover.Solutions.Dell.Html
{
    internal class RetailSmallPrice
    {
        public string Price { get; set; }

        public class Query : JQuery<RetailSmallPrice>
        {
            public Query()
                : base("Price", @"//span[@class=""retailSmallPrice""]")
            { }
        }
    }
}
