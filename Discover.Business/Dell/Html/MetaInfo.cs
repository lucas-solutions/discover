﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Html
{
    public class MetaInfo
    {
        public string CategoryPath { get; set; }

        public string Country { get; set; }

        public string CustomerSet { get; set; }

        public string Language { get; set; }

        public OpenGraph OpenGraph { get; set; }

        public string PageType { get; set; }

        public NameValueCollection Properties { get; set; }
        
        public string SalesType { get; set; }
        
        public string Segment { get; set; }

        public NameValueCollection Values { get; set; }

        public static MetaInfo Select(HtmlNode node)
        {
            var result = new MetaInfo
            {
                OpenGraph = new OpenGraph(),
                Properties = new NameValueCollection(),
                Values = new NameValueCollection()
            };

            foreach (var meta in new Meta.Query().Select(node))
            {
                var name = meta.Name != null ? meta.Name.FirstOrDefault() : null;
                var content = meta.Content != null ? meta.Content.FirstOrDefault() : null;
                var property = meta.Property != null ? meta.Property.FirstOrDefault() : null;

                if (property != null)
                {
                    result.Properties.Add(property, content);

                    var propName = property.Split(':');

                    if (propName[0] == "og" && propName.Length == 2)
                    {
                        switch (propName[1])
                        {
                            case "description":
                                result.OpenGraph.Description = content;
                                break;

                            case "image":
                                result.OpenGraph.Image = content;
                                break;

                            case "site_name":
                                result.OpenGraph.SiteName = content;
                                break;

                            case "title":
                                result.OpenGraph.Title = content;
                                break;

                            case "type":
                                result.OpenGraph.Type = content;
                                break;

                            case "url":
                                result.OpenGraph.Url = content;
                                break;
                        }
                    }
                }

                if (name != null)
                {
                    result.Values.Add(name, content);

                    switch (name)
                    {
                        case "CategoryPath":
                            result.CategoryPath = content;
                            break;

                        case "Country":
                            result.Country = content;
                            break;

                        case "CustomerSet":
                            result.CustomerSet = content;
                            break;

                        case "Language":
                            result.Language = content;
                            break;

                        case "PageType":
                            result.PageType = content;
                            break;

                        case "SalesType":
                            result.SalesType = content;
                            break;

                        case "Segment":
                            result.Segment = content;
                            break;
                    }
                }
            }

            return result;
        }
    }
}
