﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Html
{
    /// <summary>
    /// meta property="og:image" content="http://example.com/lamb-full.jpg"
    /// meta property="og:image:type" content="image/jpeg"
    /// meta property="og:image:width" content="3523"
    /// meta property="og:image:height" content="2372"
    /// meta property="og:image" content="http://example.com/logo.jpg"
    /// meta property="og:image:type" content="image/png"
    /// meta property="og:image:width" content="1024"
    /// meta property="og:image:height" content="1024"
    /// </summary>
    public class OpenGraphImage
    {
        public string Height { get; set; }

        public string Type { get; set; }

        public string Url { get; set; }

        public string Width { get; set; }
    }
}
