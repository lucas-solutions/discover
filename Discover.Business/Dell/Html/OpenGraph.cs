﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Html
{
    public class OpenGraph
    {
        /// <summary>
        /// meta property="og:description" content="Lamb with fava beans and black trumpet mushrooms"
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// meta property="og:image" content="http://example.com/lamb-full.jpg"
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// meta property="og:locale" content="en_US"
        /// </summary>
        public string Locale { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// meta property="og:url" content="http://example.com/lamb"
        /// </summary>
        public string Url { get; set; }
    }
}
