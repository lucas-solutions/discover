﻿using Discover.Solutions.Json.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Discover.Solutions.Dell.Html
{
    internal class Meta
    {
        public string[] Content { get; set; }

        public string[] Name { get; set; }

        public string[] Property { get; set; }

        public class Query : JQuery<Meta>
        {
            public Query()
                : base(null, "//html/head/meta",
                    new JQuery("Name", "@name"),
                    new JQuery("Property", "@property"),
                    new JQuery("Content", "@content"))
            {
            }
        }
    }
}
