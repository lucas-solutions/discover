﻿using Discover.Solutions.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Templates
{
    internal class ProductDetailA : JQuery
    {
        public ProductDetailA()
        {
            XPath = "//ul#list/li";
            Fields = new Collection<JQuery>
            {
                new JQuery {Name = "Content", XPath = "div/[@class=\"content datas\"]"},
                new JQuery {Name = "PriceBlock", XPath = "div/[@class=priceBlock]"},
                new JQuery {Name = "PriceStack", XPath = "div/[@class=longPriceStack]"},
            };
        }
    }
}
