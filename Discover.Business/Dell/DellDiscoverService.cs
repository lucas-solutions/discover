﻿using Discover.Solutions.Web;
using Discover.Solutions.Web.Spider;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public class DellDiscoverService : WebSpiderBase, IDellDiscoverService
    {
        private static string _archiveFolder;

        public static DirectoryInfo ArchiveDirectory
        {
            get
            {
                if (_archiveFolder == null) ArchiveDirectory = null;

                return new DirectoryInfo(_archiveFolder);
            }
            set
            {
                var directory = value ?? new DirectoryInfo(Path.Combine(Environment.CurrentDirectory, "Archive"));

                if (!directory.Exists) directory.Create();

                _archiveFolder = directory.FullName;
            }
        }

        private IDellDiscoverContext _dellDiscoverContext;

        public DellDiscoverService(WebSpiderProject project)
            : base(project)
        {
        }

        public IDellDiscoverContext Context
        {
            get { return _dellDiscoverContext ?? (_dellDiscoverContext = IoC.GetInstance<IDellDiscoverContext>()); }
            set { _dellDiscoverContext = value; }
        }

        protected override byte Rate(WebSpiderEntry entry)
        {
            var segments = entry.RemoteUrl.Split('/');

            if (segments.Contains("p")) entry.Priority = (byte)Math.Max(255, entry.Priority + 200);

            return entry.Priority;
        }

        protected override IResult Process(WebSpiderEntry entry, HtmlDocument document)
        {
            var scanner = new DellHtmlScanner();

            try
            {

                foreach (var product in scanner.Scan(entry, document))
                {
                    Context.DellProductEntityStore.Save(product);
                }
            }
            catch (Exception e)
            {
                var message = e.ToString();
            }

            return base.Process(entry, document);
        }
    }
}
