﻿using Discover.Solutions.Web.Spider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell
{
    public static class DellWebSpiderProjects
    {
        public static WebSpiderProject UsHome = new WebSpiderProject { BaseUrl = "http://www.dell.com", MaximumEntryLevel = 10, ProxyUsage = DownloadProxyUsage.NoProxy, StayOnSite = true };

        public static WebSpiderProject CaHome = new WebSpiderProject { BaseUrl = "http://www1.ca.dell.com", MaximumEntryLevel = 10, ProxyUsage = DownloadProxyUsage.NoProxy, StayOnSite = true };

        public static WebSpiderProject CaAccessories = new WebSpiderProject { BaseUrl = "http://accessories.dell.com", MaximumEntryLevel = 10, ProxyUsage = DownloadProxyUsage.NoProxy, StayOnSite = true };

        public static WebSpiderProject UsAccessories = new WebSpiderProject { BaseUrl = "http://accessories.us.dell.com", MaximumEntryLevel = 10, ProxyUsage = DownloadProxyUsage.NoProxy, StayOnSite = true };
    }
}
