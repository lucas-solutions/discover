﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Html
{
    [TestClass]
    public class ProductTests
    {
        const string ProductHtml = @"<html><head></head>"
            + @"<body>"
            + @"<div class=""starting"">"
            + @"<span>"
            + @"<div class="""">"
            + @"<div class=""shortPriceStack"">"
            + @"<span class=""startText"">Starting at</span>"
            + @"<span class=""retailSmallPrice"">"
            + @"<sup>$</sup><span class=""price"">1249<sup><span class=""hidden"">.</span>99</sup></span>"
            + @"</span>"
            + @"</div>"
            + @"<div>"
            + @"<span class=""additionalFields""></span>"
            + @"<span id=""Label5"" class=""additionalFields""></span>"
            + @"</div>"
            + @"</div>"
            + @"</span>"
            + @"</div>"
            + @"</body>"
            + @"</html>";

        [TestMethod]
        public void TestProduct()
        {
            var document = new HtmlAgilityPack.HtmlDocument();

            document.LoadHtml(ProductHtml);

            var select = new Product.Query().Select(document.DocumentNode);

            foreach (var product in select)
            {
                Assert.IsNotNull(product.StartingAt);

                Console.WriteLine(product.StartingAt.Single());
            }
        }
    }
}
