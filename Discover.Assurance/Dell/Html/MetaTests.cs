﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Dell.Html
{
    [TestClass]
    public class MetaTests
    {
        const string MetaHtml = @"<html xmlns=""http://www.w3.org/1999/xhtml"" xml:lang=""en"" xmlns:fb=""http://www.facebook.com/2008/fbml"" xmlns:og=""http://opengraphprotocol.org/schema/"" itemscope="""" itemtype=""http://schema.org/Product"" lang=""en"" dir=""ltr"" class=""no-js""><head id=""Head1"">"
        + @"<meta http-equiv=""X-UA-Compatible"" content=""IE=Edge,chrome=1"">"
        + @"<title> Inspiron  15 7000 Series 15â€ Laptop with HD Screen | Dell </title>"
        + @"<meta name=""verify-v1"" content=""google_GUI_for_Browse"">"
        + @"<meta name=""Title"" content=""New Inspiron 15 7000 Series Touch"">"
        + @"<meta name=""Description"" content=""Find a perfect blend of looks and multimedia performance in the Inspiron 15 7000 Series, featuring a premium design, backlit keyboard and optional touch screen."">"
        + @"<meta name=""Keywords"" content=""Inspiron, Inspiron 15, Inspiron 15 7000, Inspiron laptop, Inspiron touch laptop, New Inspiron 15, touch screen"">"
        + @"<meta name=""MetricsPath"" content=""&amp;eiwatch=http://www.dell.com/us/p/inspiron-15-7547-laptop/pd"">"
        + @"<meta name=""Generator"" content=""NG Content Publishing Engine v4.21.2014.0001"">"
        + @"<meta name=""s_account"" content=""dellglobalonline"">"
        + @"<meta name=""learnDomain"" content=""content.dell.com"">"
        + @"<meta property=""og:title"" content=""New Inspiron 15 7000 Series Touch"">"
        + @"<meta property=""og:url"" content=""http://www.dell.com/us/p/inspiron-15-7547-laptop/pd"">"
        + @"<meta property=""og:site_name"" content=""Dell"">"
        + @"<meta property=""fb:app_id"" content=""186850318029150"">"
        + @"<meta property=""og:description"" content=""Find a perfect blend of looks and multimedia performance in the Inspiron 15 7000 Series, featuring a premium design, backlit keyboard and optional touch screen."">"
        + @"<meta property=""og:image"" content=""http://i.dell.com/images/global/brand/ui/storm80/logo80.png"">"
        + @"<meta property=""multioc"" name=""multioc"" content=""1"">"
        + @"<meta property=""og:type"" content=""product"">"
        + @"<meta name=""CategoryPath"" content=""all-products/laptops/inspiron-laptops/inspiron-7000-laptops/inspiron-15-7547-laptop/inspiron-15-7547-laptop"">"
        + @"<meta name=""PageType"" content=""product"">"
        + @"<meta name=""SalesType"" content=""franchise"">"
        + @"<meta name=""Country"" content=""us"">"
        + @"<meta name=""Language"" content=""en"">"
        + @"<meta name=""Segment"" content=""dhs"">"
        + @"<meta name=""CustomerSet"" content=""19"">"
        + @"<meta name=""DOCUMENTCOUNTRYCODE"" content=""us"">"
        + @"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">"
        + @"<meta name=""robots"" content="""">"
        + @"<meta name=""mbox-to"" content=""5000"">"
        + @"<link rel=""canonical"" href=""http://www.dell.com/us/p/inspiron-15-7547-laptop/pd"">"
        + @"</head>"
        + @"<body class=""en "">"
        + @"<div id=""mbox_default""></div>"																																																														
        + @"</body></html>";

        [TestMethod]
        public void TestProductMeta()
        {
            var document = new HtmlAgilityPack.HtmlDocument();

            document.LoadHtml(MetaHtml);

            var meta = MetaInfo.Select(document.DocumentNode);

            Assert.IsNotNull(meta.CategoryPath);
            Assert.IsNotNull(meta.Country);
            Assert.IsNotNull(meta.CustomerSet);
            Assert.IsNotNull(meta.Language);
            Assert.IsNotNull(meta.OpenGraph);
            Assert.IsNotNull(meta.OpenGraph.Description);
            Assert.IsNotNull(meta.OpenGraph.Image);
            Assert.IsNotNull(meta.OpenGraph.SiteName);
            Assert.IsNotNull(meta.OpenGraph.Title);
            Assert.IsNotNull(meta.OpenGraph.Type);
            Assert.IsNotNull(meta.OpenGraph.Url);
            Assert.IsNotNull(meta.PageType);
            Assert.IsNotNull(meta.Properties);
            Assert.IsNotNull(meta.SalesType);
            Assert.IsNotNull(meta.Segment);
            Assert.IsNotNull(meta.Values);
        }
    }
}
