﻿using Discover.Solutions.Json.Linq;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discover.Solutions.Html
{
    [TestClass]
    public class XPathTests
    {
        [TestMethod]
        public void TestSelectElementWithClass()
        {
            var document = new HtmlDocument();

            document.LoadHtml(@"<html><head></head><body><div></div><div class=""select-me"">Success!</div></body></html>");

            var match = new JQuery(null, @"//div[@class=""select-me""]").Match(document.DocumentNode);

            Assert.IsNotNull(match);

            Assert.IsTrue(match.Succeeded);

            Assert.IsTrue(match.Value.Type == JTokenType.Array);

            var array = match.Value as JArray;

            Assert.IsTrue(array.Count == 1);

            Assert.IsTrue(array[0].Value<string>() == "Success!");
        }

        [TestMethod]
        public void TestDirectSelectElementWithClass()
        {
            var document = new HtmlDocument();

            document.LoadHtml(@"<html><head></head><body><div></div><div class=""select-me"">Success!</div></body></html>");

            var match = new JQuery(null, @"/html/body/div[@class=""select-me""]").Match(document.DocumentNode);

            Assert.IsNotNull(match);

            Assert.IsTrue(match.Succeeded);

            Assert.IsTrue(match.Value.Type == JTokenType.Array);

            var array = match.Value as JArray;

            Assert.IsTrue(array.Count == 1);

            Assert.IsTrue(array[0].Value<string>() == "Success!");
        }
    }
}
